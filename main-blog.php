<?php
$args = array(
  'posts_per_page'   => -1,
  'category_name' => 'blog',
  'post_type' => 'post',
  'post_status' => 'publish'
);

$query = new WP_Query($args);

if($query->have_posts()):
?>

<!-- Posts
============================================= -->
<div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">

  <?php while($query->have_posts()) : $query->the_post(); ?>

    <div class="entry clearfix">
      <div class="entry-image">
        <a href="<?php the_permalink(); ?>" data-lightbox="none"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php echo get_the_title(); ?>"></a>
      </div>
      <div class="entry-title">
        <h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
      </div>
      <ul class="entry-meta clearfix">
        <li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
		  <?php 
		  $author = get_field('autor-blog') != "" ? get_field('autor-blog') : "";
		  ?>
		<li><i class="icon-user"></i> <?php echo $author != "" ? $author : get_the_author(); ?></li>
      </ul>
      <div class="entry-content">
        <p><?php the_excerpt(); ?></p>
        <a href="<?php the_permalink(); ?>" class="more-link">Leer más</a>
      </div>
    </div>

  <?php endwhile; ?>

</div><!-- #posts end -->

<?php endif; ?>
