<div class="parallax header-stick bottommargin-lg dark" style="padding: 0; background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/calendario/calendar.jpg');background-size: cover;background-repeat: repeat;height: auto; margin: auto -20%; margin-top: 0 !important;" data-stellar-background-ratio="0.5">

    <div class="container clearfix">

      <div class="promocionesPresenciales" style="col_half">
        <?php
        $titulo_1 = get_option("cm-promo-calendario-titulo-1");
    		$enfasis_1 = get_option("cm-promo-calendario-enfasis-1");
    		$extra_1 = get_option("cm-promo-calendario-extra-1");
    		$titulo_2 = get_option("cm-promo-calendario-titulo-2");
    		$enfasis_2 = get_option("cm-promo-calendario-enfasis-2");
    		$extra_2 = get_option("cm-promo-calendario-extra-2");
         ?>
        <h1><?php echo $titulo_1; ?> <br><span style="font-size:25px; color:#7eb31f;"> <?php echo $enfasis_1; ?></span></h1>
        <?php if(!empty($extra_1)){ ?><blockquote><?php echo $extra_1; ?></blockquote><?php } ?>

        <div style="border:1px solid #fff;"></div>

        <h1><?php echo $titulo_2; ?> <br><span style="font-size:25px; color:#7eb31f;"> <?php echo $enfasis_2; ?></span></h1>
        <?php if(!empty($extra_2)){ ?><blockquote><?php echo $extra_2; ?></blockquote><?php } ?>
      </div>

      <div class="events-calendar" style="col_half col_last">
        <div class="events-calendar-header clearfix">
          <h2>Calendario</h2>
          <h3 class="calendar-month-year">
            <span id="calendar-month" class="calendar-month"></span>
            <span id="calendar-year" class="calendar-year"></span>
            <nav>
              <span id="calendar-prev" class="calendar-prev"><i class="icon-chevron-left"></i></span>
              <span id="calendar-next" class="calendar-next"><i class="icon-chevron-right"></i></span>
              <span id="calendar-current" class="calendar-current" title="Ir a fecha actual"><i class="icon-reload"></i></span>
            </nav>
          </h3>
        </div>
        <div id="calendar" class="fc-calendar-container"></div>
      </div>


    </div>

  </div>

  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/calendario.js.php"></script>

  <script type="text/javascript">
  	var cmEvents = {
<?php
$date1 = date('Y-m-', time());
$date2 = date('\TH:i:s', time());
//$date = date('Y-m-d\TH:i:s', time());
$date = $date1 . "01" . $date2;
$url = "https://www.googleapis.com/calendar/v3/calendars/casamaestraprado@gmail.com/events?key=AIzaSyDbJ608VWX5I-ERmqEf6zKtSCvcPVodg5w&timeMin=".$date."Z";

//  Initiate curl
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);
$cal = json_decode($result, true);

$aux_cal = array();
foreach ($cal["items"] as $event) {
	if($event["status"] == "confirmed"){
		$originalDate = isset($event["start"]["dateTime"]) ? $event["start"]["dateTime"] : $event["start"]["date"];
        $newDate = date("m-d-Y", strtotime($originalDate));
		if(count($aux_cal['items']) > 0){
			$cal_flag = false;
			$index = 0;
			foreach ($aux_cal["items"] as $item){
				if($item['date'] == ($newDate . "")){
					$aux_cal['items'][$index]['url'] .= '<a href="' . $event["htmlLink"] . '" target="_blank">' . $event["summary"] . '</a>';
					$cal_flag = true;
				}
				$index++;
			}
			if(!$cal_flag){
				$aux_cal['items'][] = array(
					'date' => $newDate,
					'url' => '<a href="' . $event["htmlLink"] . '" target="_blank">' . $event["summary"] . '</a>'
				);
			}
		} else {
			$aux_cal['items'][] = array(
				'date' => $newDate,
				'url' => '<a href="' . $event["htmlLink"] . '" target="_blank">' . $event["summary"] . '</a>'
			);
		}
	}
}
?>

  		<?php foreach ($aux_cal["items"] as $event) { ?>
        <?php
        ?>
    		'<?php echo $event['date']; ?>' : '<?php echo $event['url'] ?>',
  		<?php
    } ?>
  	};

		var cal = jQuery( '#calendar' ).calendario( {
				onDayClick : function( $el, $contentEl, dateProperties ) {

					for( var key in dateProperties ) {
						console.log( key + ' = ' + dateProperties[ key ] );
					}

				},
				caldata : cmEvents
			} ),
			$month = jQuery( '#calendar-month' ).html( cal.getMonthName() ),
			$year = jQuery( '#calendar-year' ).html( cal.getYear() );

		jQuery( '#calendar-next' ).on( 'click', function() {
			cal.gotoNextMonth( updateMonthYear );
		} );
		jQuery( '#calendar-prev' ).on( 'click', function() {
			cal.gotoPreviousMonth( updateMonthYear );
		} );
		jQuery( '#calendar-current' ).on( 'click', function() {
			cal.gotoNow( updateMonthYear );
		} );

		function updateMonthYear() {
			$month.html( cal.getMonthName() );
			$year.html( cal.getYear() );
		};

	</script>
