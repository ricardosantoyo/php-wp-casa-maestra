<?php
/* Short and sweet */
define('WP_USE_THEMES', false);
require('../../../../wp-blog-header.php');

require_once(get_template_directory().'/include/phpmailer/PHPMailerAutoload.php');

$toemails = array();

$toemails[] = array(
				'email' => 'info@casamaestra.com.mx', // Your Email Address
				'name' => 'Casa Maestra [info@casamaestra.com.mx]' // Your Name
			);

// Form Processing Messages
$message_success = 'Hemos recibido <strong>satisfactoriamente</strong> tu mensaje y te responderemos lo más pronto posible.';

// Add this only if you use reCaptcha with your Contact Forms
$recaptcha_secret = ''; // Your reCaptcha Secret

$mail = new PHPMailer();

// If you intend you use SMTP, add your SMTP Code after this Line


if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	if( $_POST['template-contactform-email'] != '' ) {

		$name = isset( $_POST['template-contactform-name'] ) ? $_POST['template-contactform-name'] : '';
		$email = isset( $_POST['template-contactform-email'] ) ? $_POST['template-contactform-email'] : '';
		$phone = isset( $_POST['template-contactform-phone'] ) ? $_POST['template-contactform-phone'] : '';
		$service = isset( $_POST['template-contactform-service'] ) ? $_POST['template-contactform-service'] : '';
		$subject = isset( $_POST['template-contactform-subject'] ) ? $_POST['template-contactform-subject'] : '';
		$message = isset( $_POST['template-contactform-message'] ) ? $_POST['template-contactform-message'] : '';

		$ref = isset( $_POST['template-contactform-referer'] ) ? $_POST['template-contactform-referer'] : '';

		$subject = 'Mensaje de cursos (' .$ref. ')';

		$botcheck = $_POST['template-contactform-botcheck'];

		if( $botcheck == '' ) {

			$mail->SetFrom( $email , $name );
			$mail->AddReplyTo( $email , $name );
			foreach( $toemails as $toemail ) {
				$mail->AddAddress( $toemail['email'] , $toemail['name'] );
			}
			$mail->Subject = $subject;

			$name = isset($name) ? "Nombre: $name<br><br>" : '';
			$email = isset($email) ? "Email: $email<br><br>" : '';
			$phone = isset($phone) ? "Teléfono: $phone<br><br>" : '';
			$service = isset($service) ? "Servicio: $service<br><br>" : '';
			$message = isset($message) ? "Mensaje: $message<br><br>" : '';

			$referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>Enviado desde la página: ' . $_SERVER['HTTP_REFERER'] : '';

			$body = "$name $email $phone $service $message $referrer";

      $body = utf8_decode($body);

			// Runs only when File Field is present in the Contact Form
			if ( isset( $_FILES['template-contactform-file'] ) && $_FILES['template-contactform-file']['error'] == UPLOAD_ERR_OK ) {
				$mail->IsHTML(true);
				$mail->AddAttachment( $_FILES['template-contactform-file']['tmp_name'], $_FILES['template-contactform-file']['name'] );
			}

			// Runs only when reCaptcha is present in the Contact Form
			if( isset( $_POST['g-recaptcha-response'] ) ) {
				$recaptcha_response = $_POST['g-recaptcha-response'];
				$response = file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptcha_secret . "&response=" . $recaptcha_response );

				$g_response = json_decode( $response );

				if ( $g_response->success !== true ) {
					echo '{ "alert": "error", "message": "Captcha not Validated! Please Try Again." }';
					die;
				}
			}

			// Uncomment the following Lines of Code if you want to Force reCaptcha Validation

			// if( !isset( $_POST['g-recaptcha-response'] ) ) {
			// 	echo '{ "alert": "error", "message": "Captcha not Submitted! Please Try Again." }';
			// 	die;
			// }

			$mail->MsgHTML( $body );
			$sendEmail = $mail->Send();

			if( $sendEmail == true ):
				//echo '{ "alert": "success", "message": "' . $message_success . '" }';
        wp_redirect($_SERVER['HTTP_REFERER'], 302);
        exit;
			else:
				echo '{ "alert": "error", "message": "El email <strong>no se pudo enviar</strong> debido a un error inesperado. Inténtalo de nuevo más tarde." }';
			endif;
		} else {
			echo '{ "alert": "error", "message": "Bot <strong>Detectado</strong>.! Más suerte la próxima ;).!" }';
		}
	} else {
		echo '{ "alert": "error", "message": "Por favor <strong>llena</strong> todos los campos e inténtalo de nuevo." }';
	}
} else {
	echo '{ "alert": "error", "message": "Ocurrió un <strong>error inesperado</strong>. Por favor inténtalo de nuevo más tarde." }';
}

?>
