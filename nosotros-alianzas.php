<div class="container wrapper bottommargin" style="background-color: #f4f2f2;">

  <h3 class="center nobottommargin" style="margin-top: 50px; color:#702982;">Nuestras Alianzas</h3>
  <h4 class="center" style="font-weight: lighter;">Tenemos alianzas con expertos para ofrecerte calidad y eduación a la vanguardia.</h4>


  <div id="oc-clients-full" class="owl-carousel owl-carousel-full image-carousel bottommargin-sm carousel-widget" data-margin="15" data-loop="true" data-nav="false" data-autoplay="2000" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="4" data-items-lg="5" style="margin-top:50px;">

  			<div class="oc-item"><a href="http://www.nomadasarte.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/1.png" alt="Nomadas"></a></div>
  			<div class="oc-item"><a href="http://www.dantza.com.mx" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/2.png" alt="Dantza"></a></div>
  			<div class="oc-item"><a href="http://www.esteelauder.com.mx" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/3.png" alt="EsteeLauder"></a></div>
  			<div class="oc-item"><a href="http://partohumanizado.com.mx" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/4.png" alt="PartoHumanizado"></a></div>
  			<div class="oc-item"><a href="http://vivirmejor.com.mx" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/5.png" alt="Vivirmejor"></a></div>
  			<div class="oc-item"><a href="http://www.proyectodei.org" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/6.png" alt="ProyectoDei"></a></div>
  			<div class="oc-item"><a href="http://www.meifritz.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/7.png" alt="Meifritz"></a></div>
  			<div class="oc-item"><a href="http://www.animartudia.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/8.png" alt="animartudia"></a></div>
  			<div class="oc-item"><a href="https://www.aserrin.mx" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/9.png" alt="Aserrin"></a></div>
  			<div class="oc-item"><a href="http://www.babylovesfood.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/10.png" alt="BabyLovesFood"></a></div>
  			<div class="oc-item"><a href="http://cozybebe.com.mx" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/11.png" alt="Cozybebe"></a></div>
  			<div class="oc-item"><a href="http://buendiabuenanoche.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/12.png" alt="BuendiaBuenanoche"></a></div>
  			<div class="oc-item"><a href="http://www.yakunay.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/13.png" alt="Yakunay"></a></div>
  			<div class="oc-item"><a href="http://logofury.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/14.png" alt="Clients"></a></div>
  			<div class="oc-item"><a href="http://www.trixiavalle.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/15.png" alt="TrixiaValle"></a></div>
  			<div class="oc-item"><a href="http://www.vidaproyecto.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/alianzas/16.png" alt="Vida Proyecto"></a></div>


  </div>
</div>
