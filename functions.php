<?php
//GLOBAL VARIABLES
$events = array();

/*********************************************/
/************ S H O R T C O D E S ************/
/*********************************************/
function cm_paypal_button(){
	ob_start();
	?>
	<blockquote><strong>Pago en línea con PAYPAL:</strong>
		
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input name="cmd" type="hidden" value="_s-xclick" />
			<input name="hosted_button_id" type="hidden" value="4AHSS8MU9PR5C" />
			<input class="button" name="submit" type="submit" value="CLICK AQUÍ" />
			<img src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" alt="" width="1" height="1" border="0" />
		</form>
	</blockquote>
	<?php
	return ob_get_clean();
}

function cm_youtube_feed() {
	$playlist = isset($_GET['playlist']) ? $_GET['playlist'] : 'PLuUdST3aMmqniBrZYnqJxoJ13SY-z7BBv';

	$key = 'AIzaSyDbJ608VWX5I-ERmqEf6zKtSCvcPVodg5w';

	$url = add_query_arg(array(
		'part' => 'snippet',
		'maxResults' => 50,
		'playlistId' => $playlist,
		'key' => $key
	), 'https://www.googleapis.com/youtube/v3/playlistItems');

	$ch = curl_init();
  // Disable SSL verification
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$url);
  // Execute
  	$result = curl_exec($ch);
  // Closing
  	curl_close($ch);

	//$json = file_get_contents($url);

	$playlist_obj = json_decode($result, true);

	?>
	<!-- Posts
	============================================= -->
	<div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">
	<?php
	foreach ($playlist_obj['items'] as $video) {
		if($video['snippet']['title'] !== 'Deleted video'){
		?>
		<div class="entry clearfix">
			<div class="entry-image">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $video['snippet']['resourceId']['videoId']; ?>?ecver=2" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="entry-title">
				<h2><?php echo $video['snippet']['title']; ?></h2>
			</div>
		</div>
		<?php
			}
		} ?>
	</div>
	<?php

	return;
}

function cm_renta_espacio_contenido(){

  $args = array(
            "category_name" => "renta-de-espacios",
            "post_status" => "publish",
						"order" => "ASC"
          );

  $posts = get_posts($args);
	?>
	<style type="text/css">
		.nombreEspacio {
			position: absolute;
			left: 50px;
			top: 50px;
			color:#fff;
		}

		.portfolio-image i {
			font-size:35px;
			position: absolute;
			background-color: #7eb31f;
			color: #fff;
			padding: 10px;
			right: 0;
			top: 200px;
		}
	</style>
  <div class="clear"></div>

    <div id="portfolio" class="portfolio grid-container portfolio-1 portfolio-fullwidth clearfix">
						<?php
	if(!empty($posts)){
		foreach ($posts as $post) {
	  ?>
	    <article class="portfolio-item pf-media pf-icons clearfix">
	        <div class="portfolio-image">
						<h3 class="nombreEspacio" data-class-sm="hidden" data-class-xs="hidden" data-class-xxs="hidden"><?php echo $post->post_title; ?></h3>
						<i class="icon-line-circle-plus"></i>
	        	<img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="<?php echo $post->post_title; ?>">
	        </div>
	        <div class="portfolio-desc">
	          <h3><?php echo $post->post_title; ?></h3>
						<p>
		          <?php echo $post->post_content; ?>
						</p>
	          <?php
	          $medidas = get_post_meta($post->ID, 'medidas', true);

	          if(!empty($medidas)){
	          ?>
	          <p>Medidas:</p>
	          <ul class="iconlist">
	            <li><i class="icon-line2-size-fullscreen"></i><?php echo $medidas; ?></li>
	          </ul>
	          <?php } ?>
	          <br>
	          <button class="button  noleftmargin" data-toggle="modal" data-target=".bs-example-modal-lg">Rentar espacio</button>
	        </div>
	      </article>
	      <?php
		}
	} else {
		?>
		<h1>Aún no hay contenido</h1>
		<?php
	}

	?>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg"  style="background-color:#fff; color:515151;">
						<div class="modal-body" style="padding:0 !important;">
							<div class="modal-content" style="background-color:#fff; color:515151; box-shadow:none; border:none;">
								<div class="modal-header" style="background-color:#702982;">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel" style="color:#fff;">¿Quiéres rentar alguno de nuestros espacios?</h4>
								</div>
								<div class="modal-body">


						<form class="nobottommargin" id="template-contactform" name="template-contactform" action="<?php bloginfo('template_directory'); ?>/include/mailespacios.php" method="post" style="background-color:#fff; color:515151;">

							<div class="form-process"></div>

							<div class="col_one_third">
								<label for="template-contactform-name">Nombre completo <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required show-error-msg formRentarEspacios" />
							</div>

							<div class="col_one_third">
								<label for="template-contactform-email">Correo electrónico <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control formRentarEspacios" />
							</div>

							<div class="clear"></div>

							<div class="col_two_third">
		            <label for="template-contactform-phone">Teléfono <small>*</small></label>
		            <input type="phone" id="template-contactform-phone" name="template-contactform-phone" value="" class="required sm-form-control formRentarEspacios" />
		          </div>

							<div class="col_one_third col_last">
								<label for="template-contactform-service">Salones</label>
								<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control formRentarEspacios">
									<option value="">-- Selecciona una opción --</option>
									<option value="Campana">Campana</option>
									<option value="Chalupa">Chalupa</option>
									<option value="Cotorro">Cotorro</option>
									<option value="Tambor">Tambor</option>
									<option value="Usos Múltiples">Usos Múltiples</option>
								</select>
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-message">Mensaje <small>*</small></label>
								<textarea class="required sm-form-control formRentarEspacios" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
							</div>

							<div class="col_full hidden">
								<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control formRentarEspacios" />
							</div>

							<div class="col_full">
								<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">ENVIAR</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
	<?php

}

function cm_blog_func(){
	get_template_part('main', 'blog');
}

function cm_nosotros_slider(){
	get_template_part('nosotros', 'slider');
}

function cm_nosotros_alianzas(){
	get_template_part('nosotros', 'alianzas');
}

function cm_nosotros_stats(){
	get_template_part('nosotros', 'stats');
}

function cm_nosotros_testimonials(){
	get_template_part('nosotros', 'testimonials');
}

function cm_nosotros_galeria(){
	get_template_part('nosotros', 'galeria');
}

function cm_cursos_calendario(){
	wp_enqueue_script( 'jquery' );
	get_template_part('cursos', 'calendario');
}

function cm_cursos_presenciales(){
	get_template_part('cursos', 'presenciales');
}

function cm_contacto_form(){
  $atts = shortcode_atts(
      array('phone_numer' => '+554 1731477',
            'phone_numer_2' => '554 1731478'
        ), $atts
    );
  ?>
  <div class="container wrapper" style="margin-top:50px;">



          <div class="promo promo-light bottommargin" style="text-align: center; width:100%; padding-left:0; padding-right: 0;">
            <h3><i class="icon-phone3" style="margin:0 20px; font-size:30px; color:#702982;"></i>Llámanos al <span style="color:#515151;"><a href="tel:5541731477" style="color:#7eb31f;">554 1731477 </a></span></h3>


          </div>

            <div class="clear"></div>


      <h1 style="color:#702982;">Envíanos un mensaje</h1>


    	<form class="nobottommargin" id="template-contactform" name="template-contactform" action="<?php bloginfo('template_directory'); ?>/include/mailcontacto.php" method="post" style="margin-top:50px;">

                <div class="form-process"></div>

                <div class="col_one_third">
                  <label for="template-contactform-name">Nombre <small>*</small></label>
                  <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required show-error-msg" />
                </div>

                <div class="col_one_third">
                  <label for="template-contactform-email">Correo electrónico <small>*</small></label>
                  <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
                </div>

                <div class="col_one_third col_last">
                  <label for="template-contactform-phone">Teléfono</label>
                  <input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
                </div>

                <div class="clear"></div>

                <div class="col_two_third">
                  <label for="template-contactform-subject">Asunto <small>*</small></label>
                  <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
                </div>

                <div class="col_one_third col_last">
                  <label for="template-contactform-service">Servicios</label>
                  <select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
                    <option value="">— Selecciona una opción —</option>
                    <option value="Conferencias en línea">Conferencias en línea</option>
                    <option value="Cursos Presenciales">Cursos Presenciales</option>
                    <option value="Renta de Espacios">Renta de Espacios</option>

                  </select>
                </div>

                <div class="clear"></div>

                <div class="col_full">
                  <label for="template-contactform-message">Mensaje <small>*</small></label>
                  <textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
                </div>
								<div class="col_full hidden">
                  <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                </div>

                <div class="col_full">
                  <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Enviar</button>
                </div>

              </form>


        </div>
  <?php
}

function cm_cursos_cv($args, $content = null){
	$args = shortcode_atts(array(
		'nombre' => ''
	), $args);

	?>
	<div style="border:1px solid #515151; width: 100%; padding:20px 30px 20px 20px;">

		<h4>Conduce:</h4>
		<p style="font-size:25px; line-height:10px; color:#515151;"><?php echo $args["nombre"]; ?></p>
		<?php if($content != null && $content != "") { ?>
		<button data-toggle="modal" data-target="#modalCV" class="btn btn-danger" style="padding-left: 50px; padding-right: 50px;">CV</button>
		<?php } ?>

	</div>

	<div class="modal fade" id="modalCV" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-body">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel"><?php echo $args["nombre"]; ?></h4>
					</div>
					<div class="modal-body">
						<p><?php echo $content; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
}

function cm_tc_func(){
	get_template_part('cm', 'terminosycondiciones');
}

function cm_ap_func(){
	get_template_part('cm', 'avisodeprivacidad');
}

add_shortcode( 'cm_blog','cm_blog_func' );
add_shortcode( 'cm_paypal_button','cm_paypal_button' );
add_shortcode( 'cm_aviso_de_privacidad','cm_ap_func' );
add_shortcode( 'cm_terminos_y_condiciones','cm_tc_func' );
add_shortcode( 'cm_contacto_form','cm_contacto_form' );
add_shortcode( 'cm_cursos_cv','cm_cursos_cv' );
add_shortcode( 'cm_cursos_calendario', 'cm_cursos_calendario' );
add_shortcode( 'cm_cursos_presenciales', 'cm_cursos_presenciales' );
add_shortcode( 'cm_nosotros_galeria', 'cm_nosotros_galeria' );
add_shortcode( 'cm_nosotros_testimonials', 'cm_nosotros_testimonials' );
add_shortcode( 'cm_nosotros_stats', 'cm_nosotros_stats' );
add_shortcode( 'cm_nosotros_alianzas', 'cm_nosotros_alianzas' );
add_shortcode( 'cm_nosotros_slider', 'cm_nosotros_slider' );
add_shortcode( 'cm_renta_de_espacios', 'cm_renta_espacio_contenido' );
add_shortcode( 'cm_youtube_feed', 'cm_youtube_feed');
/*********************************************/
/******** E N D - S H O R T C O D E S ********/
/*********************************************/
/** Step 2 (from text above). */
add_action( 'admin_menu', 'cm_theme_menu' );

/** Step 1. */
function cm_theme_menu() {
	add_menu_page( 'Ajustes del Tema', 'Casa Maestra', 'manage_options', 'ajustes-del-tema', 'cm_menu_options' );
  add_submenu_page('ajustes-del-tema', 'Slider principal', 'Slider principal', 'manage_options', 'slider-options', 'slider_options' );
	//add_submenu_page('ajustes-del-tema', 'Pagos', 'Configuración de pagos', 'manage_options', 'pagos', 'pagos_options' );
	add_submenu_page('ajustes-del-tema', 'Promociones', 'Promociones Calendario', 'manage_options', 'promociones-calendario', 'promo_calendario_func' );
}

/** Step 3. */
function cm_menu_options() {

    //must check that the user has the required capability
    if (!current_user_can('manage_options'))
    {
      wp_die( __('No tienes los suficientes permisos para estar en esta página.') );
    }

    // Add the color picker css file
    wp_enqueue_style( 'wp-color-picker' );

    // Include our custom jQuery file with WordPress Color Picker dependency
    wp_enqueue_script( 'custom-script-handle', plugins_url( 'custom-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );

    $hidden_field_name = 'mt_submit_hidden';

    // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'
    if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {

    ?>
    <div class="updated"><p><strong><?php _e('Cambios guardados.', 'menu-test' ); ?></strong></p></div>

    <?php

        }

        // Now display the settings editing screen

        echo '<div class="wrap" style="background-color:white;padding: 0 5%;">';

        // header

        echo "<h2>" . __( 'AJUSTES CASA MAESTRA', 'menu-test' ) . "</h2>";

        // settings form
				?>
				<form name="form1" method="post" action="">
					<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
				<?php

				//SETTINGS FUNCTIONS

				logotipo_del_tema();
				cm_redes_options();
				cm_promo_options();
				cm_tabs_promos_1();
				cm_tabs_promos_2();
				cm_video_principal();

				//******** SETTINGS FUNCTIONS *******//

        ?>

		<p class="submit">
			<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
		</p>

    </form>
    </div>

    <script>
    (function( $ ) {

        // Add Color Picker to all inputs that have 'color-field' class
        $(function() {
            $('.color-picker').wpColorPicker();
        });

    })( jQuery );
    </script>

    <?php
}

function cm_video_principal() {
	$cm_video = get_option("cm-video-principal");
	if(isset($_POST["cm-video-principal"])){
		$cm_video = $_POST["cm-video-principal"];
		update_option("cm-video-principal", $cm_video);
	}
	?>
	<h2>Video principal</h2>
	URL
	<input type="text" name="cm-video-principal" value="<?php echo $cm_video; ?>" size="50">
	<small>link del video de YouTube</small>

	<hr />
	<?php
}

function cm_promo_options() {
	$cm_promo = get_option("cm-promo");
	$cm_promo_color = get_option("cm-promo-color");
	$cm_promo_text = get_option("cm-promo-text");

	if(isset($_POST["cm-promo"])){
		$cm_promo = $_POST["cm-promo"];
		update_option("cm-promo", $cm_promo);
	}
	if(isset($_POST["cm-promo-color"])){
		$cm_promo_color = $_POST["cm-promo-color"];
		update_option("cm-promo-color", $cm_promo_color);
	}
	if(isset($_POST["cm-promo-text"])){
		$cm_promo_text = $_POST["cm-promo-text"];
		update_option("cm-promo-text", $cm_promo_text);
	}
	?>
	<h2>Franja promocional</h2>
	Texto de la franja
	<input type="text" name="cm-promo" value="<?php echo $cm_promo; ?>" size="50">

	<p>Color de la franja</p>
	<div class="color">
	<p>
	<input type="text" id="cm-promo-color" name="cm-promo-color" value="<?php echo $cm_promo_color; ?>" class="color-picker" /> <span><strong>Actual: <font color='<?php echo $cm_promo_color; ?>' style="border: 1px solid black;background-color:<?php echo $cm_promo_color; ?>;"> <?php echo $cm_promo_color; ?></font></strong></span>
	</p>
	</div>

	<p>Color del texto</p>
	<div class="color">
	<p>
	<input type="text" id="cm-promo-text" name="cm-promo-text" value="<?php echo $cm_promo_text; ?>" class="color-picker" /><span><strong>Actual: <font color='<?php echo $cm_promo_text; ?>' style="border: 1px solid black;background-color:<?php echo $cm_promo_text; ?>;"> <?php echo $cm_promo_text; ?></font></strong></span>
	</p>
	</div>

	<hr />
	<?php
}

function cm_redes_options() {
	$cm_fb = get_option("cm-facebook");
	$cm_tw = get_option("cm-twitter");
	$cm_ig = get_option("cm-instagram");
	$cm_yt = get_option("cm-youtube");

	if(isset($_POST["cm-facebook"])){
		$cm_fb = $_POST["cm-facebook"];

		update_option("cm-facebook", $cm_fb);
	}
	if(isset($_POST["cm-twitter"])){
		$cm_tw = $_POST["cm-twitter"];

		update_option("cm-twitter", $cm_tw);
	}
	if(isset($_POST["cm-instagram"])){
		$cm_ig = $_POST["cm-instagram"];

		update_option("cm-instagram", $cm_ig);
	}
	if(isset($_POST["cm-youtube"])){
		$cm_yt = $_POST["cm-youtube"];

		update_option("cm-youtube", $cm_yt);
	}
	?>
	<h2>Redes Sociales</h2>
	<table>
		<tr style="font-weight:normal; text-align:left;">
			<th>Facebook</th>
			<th>
				<input type="text" name="cm-facebook" value="<?php echo $cm_fb; ?>" size="20">
			</th>
		</tr>
		<tr style="font-weight:normal; text-align:left;">
			<th>Twitter</th>
			<th>
				<input type="text" name="cm-twitter" value="<?php echo $cm_tw; ?>" size="20">
			</th>
		</tr>
		<tr style="font-weight:normal; text-align:left;">
			<th>Instagram</th>
			<th>
				<input type="text" name="cm-instagram" value="<?php echo $cm_ig; ?>" size="20">
			</th>
		</tr>
		<tr style="font-weight:normal; text-align:left;">
			<th>Youtube</th>
			<th>
				<input type="text" name="cm-youtube" value="<?php echo $cm_yt; ?>" size="20">
			</th>
			<th>
				<p><small>URL completa</small></p>
			</th>
		</tr>
	</table>

	<hr />
	<?php
}

function cm_tabs_promos_1() {
	if(function_exists( 'wp_enqueue_media' )){
    wp_enqueue_media();
	}else{
	    wp_enqueue_style('thickbox');
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	}

	$cm_banner_1 = get_option("cm-online-banner-1");
	$cm_banner_2 = get_option("cm-online-banner-2");
	$cm_url_1 = get_option("cm-online-url-1");
	$cm_url_2 = get_option("cm-online-url-2");

	if(isset($_POST["cm-online-banner-1"])){
		$cm_banner_1 = $_POST["cm-online-banner-1"];

		update_option("cm-online-banner-1", $cm_banner_1);
	}
	if(isset($_POST["cm-online-banner-2"])){
		$cm_banner_2 = $_POST["cm-online-banner-2"];

		update_option("cm-online-banner-2", $cm_banner_2);
	}
	if(isset($_POST["cm-online-url-1"])){
		$cm_url_1 = $_POST["cm-online-url-1"];

		update_option("cm-online-url-1", $cm_url_1);
	}
	if(isset($_POST["cm-online-url-2"])){
		$cm_url_2 = $_POST["cm-online-url-2"];

		update_option("cm-online-url-2", $cm_url_2);
	}
	?>
	<h2>Banners promocionales - Conferencias en línea</h2>
	<p><strong>Imagen banner 1</strong></p>
	<img class="banner_1_online" src="<?php echo $cm_banner_1 ?>" height="100" width="auto"/>
	<br />
	<input class="banner_1_online_url" type="text" name="cm-online-banner-1" size="30" value="<?php echo $cm_banner_1 ?>">
	<a href="#" class="banner_1_online_upload">Subir imagen</a>

	<p><strong>URL banner 1 <small>(Opcional)</small></strong></p>
	<input type="text" name="cm-online-url-1" value="<?php echo $cm_url_1; ?>" size="60">

	<br />

	<p><strong>Imagen banner 2</strong></p>
	<img class="banner_2_online" src="<?php echo $cm_banner_2 ?>" height="100" width="auto"/>
	<br />
	<input class="banner_2_online_url" type="text" name="cm-online-banner-2" size="30" value="<?php echo $cm_banner_2 ?>">
	<a href="#" class="banner_2_online_upload">Subir imagen</a>

	<p><strong>URL banner 2<small>(Opcional)</small></strong></p>
	<input type="text" name="cm-online-url-2" value="<?php echo $cm_url_2; ?>" size="60">

	<hr />

	<script>
	    jQuery(document).ready(function($) {
	        $('.banner_1_online_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Custom Image',
	                button: {
	                    text: 'Upload Image'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.banner_1_online').attr('src', attachment.url);
	                $('.banner_1_online_url').val(attachment.url);

	            })
	            .open();
	        });
					$('.banner_2_online_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Imagen Personalizada',
	                button: {
	                    text: 'Subir Imagen'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.banner_2_online_logo').attr('src', attachment.url);
	                $('.banner_2_online_url').val(attachment.url);

	            })
	            .open();
	        });
	    });
	</script>
	<?php
}

function cm_tabs_promos_2() {
	if(function_exists( 'wp_enqueue_media' )){
    wp_enqueue_media();
	}else{
	    wp_enqueue_style('thickbox');
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	}

	$cm_banner_1 = get_option("cm-presencial-banner-1");
	$cm_banner_2 = get_option("cm-presencial-banner-2");
	$cm_url_1 = get_option("cm-presencial-url-1");
	$cm_url_2 = get_option("cm-presencial-url-2");

	if(isset($_POST["cm-presencial-banner-1"])){
		$cm_banner_1 = $_POST["cm-presencial-banner-1"];

		update_option("cm-presencial-banner-1", $cm_banner_1);
	}
	if(isset($_POST["cm-presencial-banner-2"])){
		$cm_banner_2 = $_POST["cm-presencial-banner-2"];

		update_option("cm-presencial-banner-2", $cm_banner_2);
	}
	if(isset($_POST["cm-presencial-url-1"])){
		$cm_url_1 = $_POST["cm-presencial-url-1"];

		update_option("cm-presencial-url-1", $cm_url_1);
	}
	if(isset($_POST["cm-presencial-url-2"])){
		$cm_url_2 = $_POST["cm-presencial-url-2"];

		update_option("cm-presencial-url-2", $cm_url_2);
	}
	?>
	<h2>Banners promocionales - Cursos Presenciales</h2>
	<p><strong>Imagen banner 1</strong></p>
	<img class="banner_1_presencial" src="<?php echo $cm_banner_1 ?>" height="100" width="auto"/>
	<br />
	<input class="banner_1_presencial_url" type="text" name="cm-presencial-banner-1" size="30" value="<?php echo $cm_banner_1 ?>">
	<a href="#" class="banner_1_presencial_upload">Subir imagen</a>

	<p><strong>URL banner 1 <small>(Opcional)</small></strong></p>
	<input type="text" name="cm-presencial-url-1" value="<?php echo $cm_url_1; ?>" size="60">

	<br />

	<p><strong>Imagen banner 2</strong></p>
	<img class="banner_2_presencial" src="<?php echo $cm_banner_2 ?>" height="100" width="auto"/>
	<br />
	<input class="banner_2_presencial_url" type="text" name="cm-presencial-banner-2" size="30" value="<?php echo $cm_banner_2 ?>">
	<a href="#" class="banner_2_presencial_upload">Subir imagen</a>

	<p><strong>URL banner 2<small>(Opcional)</small></strong></p>
	<input type="text" name="cm-presencial-url-2" value="<?php echo $cm_url_2; ?>" size="60">

	<hr />

	<script>
	    jQuery(document).ready(function($) {
	        $('.banner_1_presencial_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Custom Image',
	                button: {
	                    text: 'Upload Image'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.banner_1_presencial').attr('src', attachment.url);
	                $('.banner_1_presencial_url').val(attachment.url);

	            })
	            .open();
	        });
					$('.banner_2_presencial_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Imagen Personalizada',
	                button: {
	                    text: 'Subir Imagen'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.banner_2_presencial_logo').attr('src', attachment.url);
	                $('.banner_2_presencial_url').val(attachment.url);

	            })
	            .open();
	        });
	    });
	</script>
	<?php
}

function logotipo_del_tema() {

	if(function_exists( 'wp_enqueue_media' )){
    wp_enqueue_media();
	}else{
	    wp_enqueue_style('thickbox');
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	}

	// variables for the field and option names
	$opt_name = 'casamaestra_logo';
	$hidden_field_name = 'mt_submit_hidden';
	$data_field_name = 'casamaestra_logo';

	// Read in existing option value from database
	$opt_val = get_option( $opt_name );
	$logo_footer = get_option("cm-footer-logo");

	if( isset($_POST["casamaestra_logo"])){
		// Read their posted value
		$opt_val = $_POST[ $data_field_name ];

		// Save the posted value in the database
		update_option( $opt_name, $opt_val );
	}
	if( isset($_POST["cm-footer-logo"])){
		$logo_footer = $_POST["cm-footer-logo"];
		update_option( "cm-footer-logo", $logo_footer );
	}

	?>

	<br>
	<h2>Logotipo</h2>
	<p>Logotipo principal</p>

	<p>
      <img class="header_logo" src="<?php echo $opt_val; ?>" height="100" width="auto"/>
      <input class="header_logo_url" type="text" name="casamaestra_logo" size="60" value="<?php echo $opt_val ?>">
      <a href="#" class="header_logo_upload">Subir imagen</a>
	</p>
	<br>
	<p>Logotipo footer</p>

	<p>
      <img class="footer_logo" src="<?php echo $logo_footer; ?>" height="100" width="auto"/>
      <input class="footer_logo_url" type="text" name="cm-footer-logo" size="60" value="<?php echo $logo_footer ?>">
      <a href="#" class="footer_logo_upload">Subir imagen</a>
	</p>


	<script>
	    jQuery(document).ready(function($) {
	        $('.header_logo_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Custom Image',
	                button: {
	                    text: 'Upload Image'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.header_logo').attr('src', attachment.url);
	                $('.header_logo_url').val(attachment.url);

	            })
	            .open();
	        });
					$('.footer_logo_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Imagen Personalizada',
	                button: {
	                    text: 'Subir Imagen'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.footer_logo').attr('src', attachment.url);
	                $('.footer_logo_url').val(attachment.url);

	            })
	            .open();
	        });
	    });
	</script>

	<hr />
	<?php
}

function slider_options() {
	if (!current_user_can('manage_options'))
	{
		wp_die( __('No tienes los suficientes permisos para estar en esta página.') );
	}

	$hidden_field_name = 'mt_submit_hidden';

	// See if the user has posted us some information
	// If they did, this hidden field will be set to 'Y'
	if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
		?>
    <div class="updated"><p><strong><?php _e('Cambios guardados.', 'menu-test' ); ?></strong></p></div>
		<?php
	}

	?>
	<div class="wrap" style="background-color:white;padding: 0 5%;">
		<?php
		echo "<h2>" . __( 'OPCIONES SLIDER PRINCIPAL - PROMOCIÓN', 'menu-test' ) . "</h2>";

		if(function_exists( 'wp_enqueue_media' )){
	    wp_enqueue_media();
		}else{
		    wp_enqueue_style('thickbox');
		    wp_enqueue_script('media-upload');
		    wp_enqueue_script('thickbox');
		}

		$cm_slider_image = get_option("cm-slider-image");
		$cm_slider_text_1 = get_option("cm-slider-text-1");
		$cm_slider_text_2 = get_option("cm-slider-text-2");
		$cm_slider_button = get_option("cm-slider-button");
		$cm_slider_button_text = get_option("cm-slider-button-text");

		if( isset($_POST["cm-slider-image"])){
			$cm_slider_image = $_POST[ "cm-slider-image" ];
			update_option( "cm-slider-image" , $cm_slider_image );
		}
		if( isset($_POST["cm-slider-text-1"])){
			$cm_slider_text_1 = $_POST[ "cm-slider-text-1" ];
			update_option( "cm-slider-text-1" , $cm_slider_text_1 );
		}
		if( isset($_POST["cm-slider-text-2"])){
			$cm_slider_text_2 = $_POST[ "cm-slider-text-2" ];
			update_option( "cm-slider-text-2" , $cm_slider_text_2 );
		}
		if( isset($_POST["cm-slider-button"])){
			$cm_slider_button = $_POST[ "cm-slider-button" ];
			update_option( "cm-slider-button" , $cm_slider_button );
		}
		if( isset($_POST["cm-slider-button-text"])){
			$cm_slider_button_text = $_POST[ "cm-slider-button-text" ];
			update_option( "cm-slider-button-text" , $cm_slider_button_text );
		}

		?>
		<form name="form1" method="post" action="">
			<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

			<h2>Imagen de fondo *</h2>
			<p><i>*Al menos la imagen es obligatoria para que aparezca el slide</i></p>
			<p>
		      <img class="header_logo" src="<?php echo $cm_slider_image; ?>" height="100" width="auto"/>
		      <input class="header_logo_url" type="text" name="cm-slider-image" size="60" value="<?php echo $cm_slider_image ?>">
		      <a href="#" class="header_logo_upload">Subir imagen</a>
					<p><strong>Dimensiones recomendadas:</strong> 2000 × 822</p>
			</p>

			<h2>Título</h2>
			<p>
				<input type="text" name="cm-slider-text-1" value="<?php echo $cm_slider_text_1; ?>" size="100">
			</p>

			<!--<h2>Descripción</h2>
			<p>
				<input type="text" name="cm-slider-text-2" value="<?php echo $cm_slider_text_2; ?>" size="100">
			</p>-->

			<hr />

			<h2>URL botón **</h2>
			<p>
				<input type="text" name="cm-slider-button" value="<?php echo $cm_slider_button; ?>" size="100">
			</p>

			<h2>Texto del botón **</h2>
			<p>
				<input type="text" name="cm-slider-button-text" value="<?php echo $cm_slider_button_text; ?>" size="100">
			</p>

			<p><i>**Tienen que estar ambos el texto y la URL del botón para que se despliegue en el slide</i></p>

			<p class="submit">
				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
			</p>
		</form>
	</div>

	<script>
	    jQuery(document).ready(function($) {
	        $('.header_logo_upload').click(function(e) {
	            e.preventDefault();

	            var custom_uploader = wp.media({
	                title: 'Custom Image',
	                button: {
	                    text: 'Upload Image'
	                },
	                multiple: false  // Set this to true to allow multiple files to be selected
	            })
	            .on('select', function() {
	                var attachment = custom_uploader.state().get('selection').first().toJSON();
	                $('.header_logo').attr('src', attachment.url);
	                $('.header_logo_url').val(attachment.url);

	            })
	            .open();
	        });
	    });
	</script>
	<?php
}

function pagos_options() {
	if (!current_user_can('manage_options'))
	{
		wp_die( __('No tienes los suficientes permisos para estar en esta página.') );
	}

	$hidden_field_name = 'mt_submit_hidden';

	// See if the user has posted us some information
	// If they did, this hidden field will be set to 'Y'
	if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
		?>
    <div class="updated"><p><strong><?php _e('Cambios guardados.', 'menu-test' ); ?></strong></p></div>
		<?php
	}

	?>
	<div class="wrap" style="background-color:white;padding: 0 5%;">
		<?php
		echo "<h1>" . __( 'CONFIGURACIÓN DE PAGOS - CURSOS', 'menu-test' ) . "</h1>";
		?>
		<h3>Opción 1</h3>
		<?php
		$cm_pagos_transferencia = get_option("cm-pagos-transferencia");
		$cm_pagos_escuela = get_option("cm-pagos-escuela");

		if( isset($_POST["cm-pagos-transferencia"])){
			$cm_pagos_transferencia = $_POST[ "cm-pagos-transferencia" ];
			update_option( "cm-pagos-transferencia" , $cm_pagos_transferencia );
		}

		if( isset($_POST["cm-pagos-escuela"])){
			$cm_pagos_escuela = $_POST[ "cm-pagos-escuela" ];
			update_option( "cm-pagos-escuela" , $cm_pagos_escuela );
		}

		?>
		<form name="form1" method="post" action="">
			<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

			<h4>Transferencia a cuenta</h4>
			<p>
		      <textarea class="header_logo_url" type="text" name="cm-pagos-transferencia"  style="width: 500px;height: 100px;"><?php echo $cm_pagos_transferencia ?></textarea>
			</p>
			<h4>Pago en escuela</h4>
			<p>
		      <textarea class="header_logo_url" type="text" name="cm-pagos-escuela"  style="width: 500px;height: 100px;"><?php echo $cm_pagos_escuela ?></textarea>
			</p>
			<p class="submit">
				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
			</p>
		</form>
	</div>
		<?php
}

function promo_calendario_func() {
	if (!current_user_can('manage_options'))
	{
		wp_die( __('No tienes los suficientes permisos para estar en esta página.') );
	}

	$hidden_field_name = 'mt_submit_hidden';

	// See if the user has posted us some information
	// If they did, this hidden field will be set to 'Y'
	if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
		?>
    <div class="updated"><p><strong><?php _e('Cambios guardados.', 'menu-test' ); ?></strong></p></div>
		<?php
	}

	?>
	<div class="wrap" style="background-color:white;padding: 0 5%;">
		<?php
		echo "<h1>" . __( 'PROMOCIONES DEL CALENDARIO - CURSOS', 'menu-test' ) . "</h1>";
		?>
		<?php
		$titulo_1 = get_option("cm-promo-calendario-titulo-1");
		$enfasis_1 = get_option("cm-promo-calendario-enfasis-1");
		$extra_1 = get_option("cm-promo-calendario-extra-1");
		$titulo_2 = get_option("cm-promo-calendario-titulo-2");
		$enfasis_2 = get_option("cm-promo-calendario-enfasis-2");
		$extra_2 = get_option("cm-promo-calendario-extra-2");

		if( isset($_POST["cm-promo-calendario-titulo-1"])){
			$titulo_1 = $_POST[ "cm-promo-calendario-titulo-1" ];
			update_option( "cm-promo-calendario-titulo-1" , $titulo_1 );
		}

		if( isset($_POST["cm-promo-calendario-enfasis-1"])){
			$enfasis_1 = $_POST[ "cm-promo-calendario-enfasis-1" ];
			update_option( "cm-promo-calendario-enfasis-1" , $enfasis_1 );
		}

		if( isset($_POST["cm-promo-calendario-extra-1"])){
			$extra_1 = $_POST[ "cm-promo-calendario-extra-1" ];
			update_option( "cm-promo-calendario-extra-1" , $extra_1 );
		}

		if( isset($_POST["cm-promo-calendario-titulo-2"])){
			$titulo_2 = $_POST[ "cm-promo-calendario-titulo-2" ];
			update_option( "cm-promo-calendario-titulo-2" , $titulo_2 );
		}

		if( isset($_POST["cm-promo-calendario-enfasis-2"])){
			$enfasis_2 = $_POST[ "cm-promo-calendario-enfasis-2" ];
			update_option( "cm-promo-calendario-enfasis-2" , $enfasis_2 );
		}

		if( isset($_POST["cm-promo-calendario-extra-2"])){
			$extra_2 = $_POST[ "cm-promo-calendario-extra-2" ];
			update_option( "cm-promo-calendario-extra-2" , $extra_2 );
		}

		?>
		<form name="form1" method="post" action="">
			<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
			<h3>Promoción 1</h3>
			<h4>Título</h4>
			<p>
		      <input class="header_logo_url" type="text" name="cm-promo-calendario-titulo-1" size="60" value="<?php echo $titulo_1; ?>" />
			</p>
			<h4>Texto en énfasis</h4>
			<p>
		      <input class="header_logo_url" type="text" name="cm-promo-calendario-enfasis-1" size="60" value="<?php echo $enfasis_1; ?>" />
			</p>
			<h4>Texto extra</h4>
			<p>
		      <input class="header_logo_url" type="text" name="cm-promo-calendario-extra-1" size="60" value="<?php echo $extra_1; ?>" />
			</p>
			<hr />

			<h3>Promoción 2</h3>
			<h4>Título</h4>
			<p>
		      <input class="header_logo_url" type="text" name="cm-promo-calendario-titulo-2" size="60" value="<?php echo $titulo_2; ?>" />
			</p>
			<h4>Texto en énfasis</h4>
			<p>
		      <input class="header_logo_url" type="text" name="cm-promo-calendario-enfasis-2" size="60" value="<?php echo $enfasis_2; ?>" />
			</p>
			<h4>Texto extra</h4>
			<p>
		      <input class="header_logo_url" type="text" name="cm-promo-calendario-extra-2" size="60" value="<?php echo $extra_2; ?>" />
			</p>
			<p class="submit">
				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
			</p>
		</form>
	</div>
		<?php
}

add_action( 'after_setup_theme', 'blankslate_setup' );

function blankslate_setup() {
  load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'post-thumbnails' );
  global $content_width;
  if ( ! isset( $content_width ) ) $content_width = 640;
    register_nav_menus(
      array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
    );
}

add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );

function blankslate_load_scripts() {
  wp_enqueue_script( 'jquery' );
}

add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );

function blankslate_enqueue_comment_reply_script() {
  if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'blankslate_title' );

function blankslate_title( $title ) {
  if ( $title == '' ) {
    return '&rarr;';
  } else {
    return $title;
  }
}

add_filter( 'wp_title', 'blankslate_filter_wp_title' );

function blankslate_filter_wp_title( $title ) {
  return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'blankslate_widgets_init' );

function blankslate_widgets_init() {
  register_sidebar( array (
    'name' => __( 'Sidebar Widget Area', 'blankslate' ),
    'id' => 'primary-widget-area',
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));
}

function blankslate_custom_pings( $comment ) {
  $GLOBALS['comment'] = $comment;
  ?>
  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
  <?php
}

add_filter( 'get_comments_number', 'blankslate_comments_number' );

function blankslate_comments_number( $count ) {
  if ( !is_admin() ) {
    global $id;
    $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
    return count( $comments_by_type['comment'] );
  } else {
    return $count;
  }
}

function get_home_posts($offset) {

  if( is_numeric($offset) )
    $offset = ($offset != 0) ? (($offset - 1) * 10) : 0;
  else
    $offset = 0;

  ?>

  <!-- Post Content
  ============================================= -->
  <div class="nobottommargin clearfix">

    <!-- Posts
    ============================================= -->
    <div id="posts">

  <?php
  $args = array(
    'offset' => $offset,
    'category__not_in' => 1,
    'post_status' => 'publish'
  );
  $recent_posts = wp_get_recent_posts($args);

  if($recent_posts == false){
    ?>
    <div class="entry clearfix">
      <div class="entry-title">
        <h2>No hay posts por mostrar</h2>
      </div>
    </div>
    <?php
  }

  foreach( $recent_posts as $recent ) {
    ?>
    <div class="entry clearfix">
      <div class="entry-image">
        <a href="<?php echo get_the_permalink( $recent['ID'] ); ?>"><?php echo get_the_post_thumbnail($recent['ID'], 'full', array( 'class' => 'img-fade main-thumb' )); ?></a>
      </div>
      <div class="entry-title">
        <h2><a href="<?php echo get_the_permalink( $recent['ID'] ); ?>"><?php echo $recent['post_title']; ?></a></h2>
      </div>
      <ul class="entry-meta clearfix">
        <?php $date = date_create($recent['post_date']); ?>
        <li><i class="icon-calendar3"></i> <?php echo date_format($date, 'j / n / Y'); ?></li>
        <li><i class="icon-user"></i> <?php echo get_the_author_meta( 'first_name', $recent['post_author'] ) . ' ' . get_the_author_meta( 'last_name', $recent['post_author'] ); ?></li>
        <li><i class="icon-folder-open"></i> <?php
        $categories = get_the_category( $recent['ID'] );
        $cat = null;
        foreach ($categories as $category) {
          $cat[] = $category->name;
        }
        $cat_string = implode($cat, ", ");
        echo $cat_string;
        ?></li>
        <li><a href="<?php echo get_the_permalink( $recent['ID'] ); ?>#comment-section"><i class="icon-comments"></i> <?php echo $recent['comment_count']; ?></a></li>
      </ul>
      <div class="entry-content">
        <p><?php echo the_excerpt(); ?></p>
        <a href="<?php echo get_the_permalink( $recent['ID'] ); ?>"class="more-link">Leer más...</a>
      </div>
    </div>

    <?php
  }

  ?>
    </div>
  </div>
  <?php
}

function create_cm_menu( $theme_location ) {
  if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
    ?>
		<div id="header-wrap">

			<div class="container clearfix">

				<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

				<?php

				// Logoitpo
				$LOGO = get_option( 'casamaestra_logo' );

				if(!empty($LOGO)){
				?>
		<style>
			@media (min-width: 1241px){
				#header.full-header #logo {
					padding-right: 10px !important;
					margin-right: 0px !important;
					margin-left: -15px;
				}
			}
		</style>
        <!-- Logo
        ============================================= -->
        <div id="logo">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="standard-logo" data-dark-logo="<?php echo $LOGO; ?>"><img src="<?php echo $LOGO; ?>" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>"></a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="retina-logo" data-dark-logo="<?php echo $LOGO; ?>"><img src="<?php echo $LOGO; ?>" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>"></a>
        </div><!-- #logo end -->

				<?php } ?>

        <!-- Primary Navigation
        ============================================= -->
        <nav id="primary-menu" class="dark">

    <?php

    $menu = get_term( $locations[$theme_location], 'nav_menu' );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $menu_list .= '<ul>' ."\n";

    foreach( $menu_items as $menu_item ) {
        if( $menu_item->menu_item_parent == 0 ) {

            $parent = $menu_item->ID;

            $menu_array = array();
            $bool = false;
            foreach( $menu_items as $submenu ) {
                if( $submenu->menu_item_parent == $parent ) {
                    $bool = true;
                    $menu_array[] = '<li><a href="' . $submenu->url . '">' . $submenu->title . '</a></li>' ."\n";
                }
            }
            if( $bool == true && count( $menu_array ) > 0 ) {
                $menu_list .= '<li class="' . $class . '">' ."\n";
                $menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n";

                $menu_list .= '<ul>' ."\n";
                $menu_list .= implode( "\n", $menu_array );
                $menu_list .= '</ul>' ."\n";

                // end <li>
                $menu_list .= '</li>' ."\n";

            } else {
								$ID = get_the_ID();
								$class = (strtoupper($menu_item->title) === strtoupper(get_the_title())) ? ' class="current"' : "";
								$class = (is_home() && (strtoupper($menu_item->title) == "INICIO")) ? ' class="current"' : $class;
								if (strpos(strtoupper($menu_item->title), 'CONFERENCIAS') !== false) {
    								$t = true;
								} else {
									$t = false;
								}
								$target = $t ? ' target="_blank"' : ' target="_parent"';
                $menu_list .= '<li'.$class.'>' ."\n";
                $menu_list .= '<a href="' . $menu_item->url . '" '. $target. '>' . $menu_item->title . '</a>' ."\n";

                // end <li>
                $menu_list .= '</li>' ."\n";
            }

        }
    }

    $menu_list .= '</ul>' ."\n";
		?>
		<!-- Top Search
		============================================= -->
		<div id="top-search">
			<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
			<form action="/" role="search" method="get">
				<input type="text" name="s" class="form-control" value="" placeholder="Escribe algo y presiona Enter">
			</form>
		</div><!-- #top-search end -->
		<?php
    $menu_list .= '</nav><!-- #primary-menu end -->' ."\n";

    echo $menu_list;

    ?>

    </div>
    <?php
  }
}

function cm_banner_redes() {
	?>
			<!-- Top Bar
		============================================= -->
		<div id="top-bar">

		<div>

			<?php
			$cm_promo_color = get_option("cm-promo-color");
			$cm_promo_text = get_option("cm-promo-text");

			$promo_color = empty($cm_promo_color) ? '#FFFFFF' : $cm_promo_color;
			$promo_color_text = empty($cm_promo_text) ? '#000000' : $cm_promo_text;
			?>

		<div class="col-md-9" style="background-color:<?php echo $promo_color; ?>; color:<?php echo $promo_color_text; ?>;  text-align: center; ">

			<?php
			$cm_promo = get_option("cm-promo");

			if(!empty($cm_promo)){
			?>
			<!-- Top Links
			============================================= -->
			<div class="top-links">

			<ul style="background-color:<?php echo $promo_color; ?>">


				<li style="text-align: center; width:100%; font-size:18px; background-color:<?php echo $promo_color; ?>; color:<?php echo $promo_color_text; ?>;"><?php echo $cm_promo; ?></li>
			</ul>



			</div><!-- .top-links end -->
			<?php } ?>

		</div>

		<div class="col-md-3">

			<!-- Top Social
			============================================= -->
			<div id="top-social">
				<ul>
				<?php
				$cm_fb = get_option("cm-facebook");
				$cm_tw = get_option("cm-twitter");
				$cm_ig = get_option("cm-instagram");
				$cm_yt = get_option("cm-youtube");
				?>
				<?php if(!empty($cm_fb)){ ?>
					<li><a href="https://www.facebook.com/<?php echo $cm_fb; ?>" class="si-facebook" target="_blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
				<?php } ?>
				<?php if(!empty($cm_tw)){ ?>
					<li><a href="https://twitter.com/<?php echo $cm_tw; ?>" target="_blank" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
				<?php } ?>
				<?php if(!empty($cm_ig)){ ?>
					<li><a href="https://www.instagram.com/<?php echo $cm_ig; ?>" target="_blank" class="si-dribbble"><span class="ts-icon"><i class="icon-instagram"></i></span><span class="ts-text">Instagram</span></a></li>
				<?php } ?>
				<?php if(!empty($cm_yt)){ ?>
					<li><a href="https://<?php echo $cm_yt; ?>" target="_blank" class="si-youtube"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
				<?php } ?>
				</ul>
			</div><!-- #top-social end -->

		</div>

		</div>

		</div><!-- #top-bar end -->
			<?php
}

function cm_footer_redes() {
	?>
	<div class="widget clearfix">
	<?php

				$cm_fb = get_option("cm-facebook");
				$cm_tw = get_option("cm-twitter");
				$cm_ig = get_option("cm-instagram");
				$cm_yt = get_option("cm-youtube");
				?>
				<?php if(!empty($cm_fb)){ ?>
					<a href="https://www.facebook.com/<?php echo $cm_fb; ?>" target="_blank" class="social-icon si-small si-rounded si-facebook">
						<i class="icon-facebook"></i>
						<i class="icon-facebook"></i>
					</a>
				<?php } ?>
				<?php if(!empty($cm_tw)){ ?>
					<a href="https://twitter.com/<?php echo $cm_tw; ?>" target="_blank" class="social-icon si-small si-rounded si-twitter">
						<i class="icon-twitter"></i>
						<i class="icon-twitter"></i>
					</a>
				<?php } ?>
				<?php if(!empty($cm_ig)){ ?>
					<a href="https://www.instagram.com/<?php echo $cm_ig; ?>" target="_blank" class="social-icon si-small si-rounded si-instagram">
						<i class="icon-instagram"></i>
						<i class="icon-instagram"></i>
					</a>
				<?php } ?>
				<?php if(!empty($cm_yt)){ ?>
					<a href="https://<?php echo $cm_yt; ?>" target="_blank" class="social-icon si-small si-rounded si-youtube">
						<i class="icon-youtube"></i>
						<i class="icon-youtube"></i>
					</a>
				<?php } ?>

		</div>
			<?php
}

function get_related_posts( $tags_array ) {
  $orig_post = $post;
  global $post;

  $tags = $tags_array;

  $post_ID = get_the_ID();

  $tag_ids = array();
  foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
  $args=array(
    'tag__in' => $tag_ids,
    'category__not_in' => 1,
    'post__not_in' => array($post_ID),
    'posts_per_page' => 2, // Number of related posts to display.
    'caller_get_posts' => 1
  );

  $my_query = new wp_query( $args );

  ?>

  <div class="line"></div>

  <h4>Posts Relacionados:</h4>

  <div class="related-posts clearfix">

    <div class="col_half nobottommargin">

      <?php

      while( $my_query->have_posts() ) {
        $my_query->the_post();

        ?>
        <div class="mpost clearfix">
          <div class="entry-image">
            <a href="<?php the_permalink()?>"><?php the_post_thumbnail(); ?></a>
          </div>
          <div class="entry-c">
            <div class="entry-title">
              <h4><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h4>
            </div>
            <ul class="entry-meta clearfix">
              <li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
              <!-- <li><a href="#"><i class="icon-comments"></i> 12</a></li> -->
            </ul>
            <div class="entry-content"><?php the_excerpt(); ?></div>
          </div>
        </div>
        <?php
      }
      ?>

    </div>

    <div class="col_half nobottommargin col_last">

    <?php
    $args=array(
      'tag__in' => $tag_ids,
      'category__not_in' => 1,
      'post__not_in' => array($post_ID),
      'posts_per_page' => 2, // Number of related posts to display.
      'caller_get_posts' => 1,
      'offset' => 2
    );

    $my_query = new wp_query( $args );

    while( $my_query->have_posts() ) {
      $my_query->the_post();

      ?>
      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="<?php the_permalink()?>"><?php the_post_thumbnail(); ?></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
            <!-- <li><a href="#"><i class="icon-comments"></i> 12</a></li> -->
          </ul>
          <div class="entry-content"><?php the_excerpt(); ?></div>
        </div>
      </div>
      <?php
    }
    ?>

    </div>

  </div>
  <?php

  $post = $orig_post;
  wp_reset_query();
}

function cm_get_tabs() {
	?>
<style>
.owl-carousel .owl-nav [class*="owl-"] {
	opacity: 1;
}
</style>
<style>
.tabs-cm {
		width: 50%;
	}

p.comoFunciona_p {
	margin-bottom: 0 !important;
}

@media (max-width: 991px){
	.tabs-cm {
		width: 100%;
	}

	.DivComoFuncionaenLinea {
		margin-left: auto;
	}
}

.comoFuncionaenLinea {
	padding: 0;
}
</style>
	<!-- Portfolio Items
	============================================= -->

	<div class="tabs clearfix" id="tab-3" style="width: 100%; margin-bottom: 0;">

		<ul class="tab-nav tab-nav2 clearfix" style="width: 100%; padding:0; margin:0;">

			<li class="col_half tabs-cm" style="margin: 0 !important; display:inline-block;">
				<a href="#tab-1" style="font-size:20px;" data-scrollto="#tab-1"><i class="icon-line-monitor" style="font-size:20px;"></i> &nbsp &nbsp Conferencias en línea</a>
			</li>

			<li class="col_half col_last tabs-cm" style="margin: 0 !important; display: inline-block;">
				<a href="#tab-2" style="font-size:20px;" data-scrollto="#tab-2"><i class="icon-line2-graduation" style="font-size:20px;"></i>  &nbsp &nbsp Cursos Presenciales</a>
			</li>

		</ul>

		<div class="tab-container">

			<div class="tab-content clearfix" id="tab-1" style="margin-bottom:100px;">

				<!--texto de impacto-->

				<div class="container clearfix DivComoFuncionaenLinea">

					<div class="col-md-2 comoFuncionaenLinea" data-animate="swing" data-delay="500">

						<h2><i class="icon-line-clock"></i>3 Minutos</h2><br><p>para inscribirte</p>

					</div>

					<div class="col-md-2 comoFuncionaenLinea" data-animate="swing" data-delay="700">

						<h2><i class="icon-line2-mouse"></i>3 Clicks</h2><br><p>para comprar</p>

					</div>

					<div class="col-md-2 comoFuncionaenLinea" data-animate="swing" data-delay="900" style="margin-top: -15px;">

						<h2 style="line-height: normal !important;"><i class="icon-calendar-empty"></i>Acceso ilimitado</h2><br><p class="comoFunciona_p">a tu propio ritmo</p>

					</div>

					<div class="col-md-3 comoFuncionaenLinea" data-animate="swing" data-delay="1100">

						<?php
						$video = get_option('cm-video-principal');
						$video = empty($video) ? "#" : $video;
						 ?>
						<h3><a href="<?php echo $video; ?>" class="button button-xlarge" data-lightbox="iframe">¿Cómo funciona?</a></h3>

					</div>

				</div>

				<!--fin de texto de impacto -->

				<div class="owl-carousel portfolio-carousel carousel-widget" data-autoplay="true" data-loop="true" data-margin="10" data-speed="4500" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="4">

					<?php
					$args = array(
						"numberposts" => 16,
						"category_name" => "conferencias-en-linea",
						"post_status" => "publish",
						"orderby" => "title",
						"order" => "ASC"
					);

					$posts = get_posts($args);

					if($posts) {
						for($i = 0; $i < count($posts); $i=$i+2) {
						?>
							<div class="oc-item">

								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="<?php echo $posts[$i]->guid; ?>" target="_blank">
											<img src="<?php echo get_the_post_thumbnail_url($posts[$i]->ID); ?>" alt="<?php echo $posts[$i]->post_title; ?>">
										</a>
										<div class="portfolio-overlay">
											<a href="<?php echo $posts[$i]->guid; ?>" class="center-icon" target="_blank"><i class="icon-line-ellipsis"></i></a>
										</div>
									</div>
									<div class="portfolio-desc">
										<h3><a href="<?php echo $posts[$i]->guid; ?>" target="_blank"><?php echo $posts[$i]->post_title; ?></a></h3>
									</div>
								</div>

								<div class="clear"></div>

								<?php
								if(isset($posts[$i+1])) {
									?>
									<div class="iportfolio">
										<div class="portfolio-image">
											<a href="<?php echo $posts[$i+1]->guid; ?>" target="_blank">
												<img src="<?php echo get_the_post_thumbnail_url($posts[$i+1]->ID); ?>" alt="<?php echo $posts[$i+1]->post_title; ?>">
											</a>
											<div class="portfolio-overlay">
												<a href="<?php echo $posts[$i+1]->guid; ?>" target="_blank" class="center-icon"><i class="icon-line-ellipsis"></i></a>
											</div>
										</div>
										<div class="portfolio-desc">
											<h3><a href="<?php echo $posts[$i+1]->guid; ?>" target="_blank"><?php echo $posts[$i+1]->post_title; ?></a></h3>
										</div>
									</div>
									<?php
								}
								?>

							</div>

							<?php
						}//end for - posts
					}//end if - posts
					?>

				</div> <!--cierre de occ-portfolio -->

				<!--banners -->

				<div class="container-fluid clearfix topmargin" data-class-lg="container" data-class-md="container">
<?php
$banner_1_online = get_option('cm-online-banner-1');
$url_1_online = get_option('cm-online-url-1');
$banner_2_online = get_option('cm-online-banner-2');
$url_2_online = get_option('cm-online-url-2');
if(!empty($banner_1_online) && !empty($banner_2_online)){
$flag_banner = true;
} else {
$flag_banner = false;
}
$class_banner = $flag_banner ? "col-md-6" : "col-md-12";
if(!$flag_banner) { $width = "50%"; } else { $width = "100%"; }

 ?>

					<div class="banner2online <?php echo $class_banner; ?>">
						<?php
						if(!$flag_banner) { echo "<center>"; }
						echo (!empty($url_1_online) && !empty($banner_1_online)) ? '<a href="' . $url_1_online . '" >' : '';
						?>
						<?php echo !empty($banner_1_online) ? '<img src="' . $banner_1_online . '" class="img-responsive banner-tabs" style="width:' . $width . '; height: auto;">' : ''; ?>
						<?php echo (!empty($url_1_online) && !empty($url_1_online)) ? '</a>' : ''; ?>
						<?php if(!$flag_banner) { echo "</center>"; } ?>
					</div>

					<div class="banner2online <?php echo $class_banner; ?>">

						<?php
						if(!$flag_banner) { echo "<center>"; }
						echo (!empty($url_2_online) && !empty($banner_2_online)) ? '<a href="' . $url_2_online . '" >' : '';
						?>
						<?php echo !empty($banner_2_online) ? '<img src="' . $banner_2_online . '" class="img-responsive banner-tabs" style="width:' . $width . '; height: auto;">' : ''; ?>
						<?php echo (!empty($url_2_online) && !empty($url_2_online)) ? '</a>' : ''; ?>
						<?php if(!$flag_banner) { echo "</center>"; } ?>
					</div>

				</div>

				<!--fin de banners -->

			</div> <!--.tab-content #tab-1 -->

			<div class="clear"></div>

			<!--listado de cursos presenciales-->

			<div class="tab-content clearfix" id="tab-2" style="margin-bottom:100px;">

				<div class="owl-carousel portfolio-carousel carousel-widget" data-autoplay="true" data-loop="true" data-margin="10" data-speed="4500" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="4">

					<?php
					$args = array(
						"numberposts" => 16,
						"category_name" => "cursos-presenciales",
						"post_status" => "publish",
						"orderby" => "title",
						"order" => "ASC"
					);

					$posts = get_posts($args);

					if($posts) {
						for($i = 0; $i < count($posts); $i=$i+2) {
						?>
							<div class="oc-item">

								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="<?php echo $posts[$i]->guid; ?>">
											<img src="<?php echo get_the_post_thumbnail_url($posts[$i]->ID); ?>" alt="<?php echo $posts[$i]->post_title; ?>">
										</a>
										<div class="portfolio-overlay">
											<a href="<?php echo $posts[$i]->guid; ?>" class="center-icon"><i class="icon-line-ellipsis"></i></a>
										</div>
									</div>
									<div class="portfolio-desc">
										<h3><a href="<?php echo $posts[$i]->guid; ?>"><?php echo $posts[$i]->post_title; ?></a></h3>
									</div>
								</div>

								<div class="clear"></div>

								<?php
								if(isset($posts[$i+1])) {
									?>
									<div class="iportfolio">
										<div class="portfolio-image">
											<a href="<?php echo $posts[$i+1]->guid; ?>">
												<img src="<?php echo get_the_post_thumbnail_url($posts[$i+1]->ID); ?>" alt="<?php echo $posts[$i+1]->post_title; ?>">
											</a>
											<div class="portfolio-overlay">
												<a href="<?php echo $posts[$i+1]->guid; ?>" class="center-icon"><i class="icon-line-ellipsis"></i></a>
											</div>
										</div>
										<div class="portfolio-desc">
											<h3><a href="<?php echo $posts[$i+1]->guid; ?>"><?php echo $posts[$i+1]->post_title; ?></a></h3>
										</div>
									</div>
									<?php
								}
								?>

							</div>

							<?php
						}//end for - posts
					}//end if - posts
					?>

				</div> <!--cierre de occ-portfolio -->

				<!--banners -->
				<div class="container-fluid clearfix" data-class-lg="container" data-class-md="container">
<?php
$banner_1_presencial = get_option('cm-presencial-banner-1');
$url_1_presencial = get_option('cm-presencial-url-1');
$banner_2_presencial = get_option('cm-presencial-banner-2');
$url_2_presencial = get_option('cm-presencial-url-2');
if(!empty($banner_1_presencial) && !empty($banner_2_presencial)){
$flag_banner = true;
} else {
$flag_banner = false;
}
$class_banner = $flag_banner ? "col-md-6" : "col-md-12";
if(!$flag_banner) { $width = "50%"; } else { $width = "100%"; }
?>

					<div class="banner1online <?php echo $class_banner; ?>">
						<?php if(!$flag_banner){ echo "<center>"; } ?>
						<?php

						echo (!empty($url_1_presencial) && !empty($banner_1_presencial)) ? '<a href="' . $url_1_presencial . '" >' : '';
						?>
						<?php echo !empty($banner_1_presencial) ? '<img src="' . $banner_1_presencial . '" class="img-responsive banner-tabs" style="width:' . $width . '; height: auto;">' : ''; ?>
						<?php echo (!empty($url_1_presencial) && !empty($banner_1_presencial)) ? '</a>' : ''; ?>
					<?php if(!$flag_banner){ echo "</center>"; } ?>
					</div>

					<div class="banner2online <?php echo $class_banner; ?>">
						<?php if(!$flag_banner){ echo "<center>"; } ?>
						<?php
						
						
						echo (!empty($url_2_presencial) && !empty($banner_2_presencial)) ? '<a href="' . $url_2_presencial . '" >' : '';
						?>
						<?php echo !empty($banner_2_presencial) ? '<img src="' . $banner_2_presencial . '" class="img-responsive banner-tabs" style="width:' . $width . '; height: auto;">' : ''; ?>
						<?php echo (!empty($url_2_presencial) && !empty($banner_2_presencial)) ? '</a>' : ''; ?>

					</div>
					<?php if(!$flag_banner){ echo "</center>"; } ?>
				</div>

				<!--fin de banners -->

			</div> <!--.tab-content #tab-2 -->

		</div> <!-- cierre de tabs general-->
	<?php
}

function cm_fancy_title( $title = "Fancy Title", $subtitle = null, $fancy = false ){

	if(!$fancy){
		echo '<section id="page-title">'.
		    '<div class="container clearfix">'.
		      '<h1>' . $title . '</h1>';

		if($subtitle != null ) { echo '<span>' . $subtitle . '</span>'; }

		echo '</div>'.
		  '</section>';
	} else {
		?>
		<div class="clearfix"></div>
		<section>

			<div class="section parallax dark notopmargin noborder" style="padding: 80px 0; background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/presenciales/cabeceras/5.jpg');" data-stellar-background-ratio="0.4">
					<div class="container center clearfix">

						<div class="emphasis-title">
							<h2><?php echo $title; ?></h2>
							<p class="lead topmargin-sm"><?php echo $subtitle; ?></p>
						</div>

					</div>
				</div>

		</section>
		<?php
	}

}

function cm_contacto_mapa(){
	wp_enqueue_script('maps_google', 'https://maps.google.com/maps/api/js?key=AIzaSyDNRok1k6AJ5MrA-I9Mww7VeH7J3iFb0-M', array(), '1.0.0', true );
	wp_enqueue_script('gmap', get_template_directory() . '/js/jquery.gmap.js', array(), '1.0.0', true );

  $atts = array(
		"google_map_address" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d940.6620858330367!2d-99.21646935131474!3d19.42759428016027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d2018afb7f9bc1%3A0xf759a3a6ce608b4a!2sAvenida+Prado+Norte+565%2C+Lomas+-+Virreyes%2C+Lomas+de+Chapultepec+V+Secc%2C+11000+Ciudad+de+M%C3%A9xico%2C+CDMX!5e0!3m2!1ses-419!2smx!4v1495807025775",
		"height" => "450"
	);
  ?>
  <section>
      <iframe src = <?php echo $atts['google_map_address'] ?> width = "100%" height = <?php echo $atts['height'] ?> style="height: <?php echo $atts['height'] ?>px !important;" frameborder = "0" style = "border:0" allowfullscreen>
      </iframe>
  </section>
    <?php
}

function cm_blog_index_func(){
	$args = array(
		'posts_per_page' => 3,
		'category_name' => 'blog',
		'post_type' => 'post',
		'post_status' => 'publish'
	);

	$query = new WP_Query($args);

	if($query->have_posts()):
		?>

				<div class="container clearfix" style="padding-top:50px;">

					<h2 class="center">Soy mamá como tú</h3>
					<h4 class="center">Consulta todas las notas <a class="button" href="blog">aquí</a></h4>

					<div class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
						<?php while($query->have_posts()) : $query->the_post(); ?>

						<div class="entry clearfix">
							<div class="entry-image">
								<a href="<?php the_permalink(); ?>"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php echo get_the_title(); ?>"></a>
							</div>
							<div class="entry-title">
								<h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
								 <?php 
								  $author = get_field('autor-blog') != "" ? get_field('autor-blog') : "";
								  ?>
								<li><i class="icon-user"></i> <?php echo $author != "" ? $author : get_the_author(); ?></li>
							</ul>

						</div>

						<?php endwhile; ?>

					</div><!-- #posts end -->

				</div>
	<?php
	endif;
}

add_theme_support( 'create_cm_menu' );

add_action( 'comment_form_before', 'cm_enqueue_comment_reply_script' );

function cm_enqueue_comment_reply_script() {
  if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'get_comments_number', 'cm_comments_number' );

function cm_comments_number( $count ) {
  if ( !is_admin() ) {
    global $id;
    $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
    return count( $comments_by_type['comment'] );
  } else {
    return $count;
  }
}