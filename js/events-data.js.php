<?php
global $events;

if(!empty($events)) { ?>
	var canvasEvents = {
		<?php foreach ($events as $event) { ?>
		'<?php echo $event["date"]; ?>' : '<a href="<?php echo $event["url"]; ?>" target=_blank><?php echo $event["title"]; ?></a>',
		<?php } ?>
	};
<?php } else { ?>
var canvasEvents = {};
<?php } ?>
