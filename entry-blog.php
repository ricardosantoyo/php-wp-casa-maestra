<!-- Page Title
============================================= -->
<section id="page-title">

  <div class="container clearfix">
    <a href="blog"><h1>Blog</h1></a>

  </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="container clearfix">

      <div class="single-post nobottommargin">

        <!-- Single Post
        ============================================= -->
        <div class="entry clearfix">

          <!-- Entry Title
          ============================================= -->
          <div class="entry-title">
            <h2><?php the_title(); ?></h2>
          </div><!-- .entry-title end -->

          <!-- Entry Meta
          ============================================= -->
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
          </ul><!-- .entry-meta end -->

          <!-- Entry Image
          ============================================= -->
          <div class="entry-image bottommargin">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php the_title(); ?>" style="width: 50%; margin: 0 auto;">
          </div><!-- .entry-image end -->

          <!-- Entry Content
          ============================================= -->
          <div class="entry-content notopmargin">

            <p><?php the_content(); ?></p>
            <!-- Post Single - Content End -->

            <div class="clear"></div>

            <!-- Post Single - Share
            ============================================= -->
            <div class="si-share noborder clearfix">
              <span>Compartir Post:</span>
              <div>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>" class="social-icon si-borderless si-facebook">
                  <i class="icon-facebook"></i>
                  <i class="icon-facebook"></i>
                </a>
                <a href="https://twitter.com/home?status=<?php echo get_the_permalink();?>" class="social-icon si-borderless si-twitter">
                  <i class="icon-twitter"></i>
                  <i class="icon-twitter"></i>
                </a>

                <a href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>" class="social-icon si-borderless si-gplus">
                  <i class="icon-gplus"></i>
                  <i class="icon-gplus"></i>
                </a>

                <a href="mailto:?&body=<?php echo get_the_permalink();?>" class="social-icon si-borderless si-email3">
                  <i class="icon-email3"></i>
                  <i class="icon-email3"></i>
                </a>
              </div>
            </div><!-- Post Single - Share End -->

          </div>
        </div><!-- .entry end -->
		  
		<?php if ( ! post_password_required() ) comments_template( '', true ); ?>

        <!-- Post Navigation
        ============================================= -->
        <div class="post-navigation clearfix">

          <?php get_template_part( 'nav', 'below-single' ); ?>

        </div><!-- .post-navigation end -->

        <div class="line"></div>

        <!-- Post Author Info
        ============================================= -->


        <div class="line"></div>

        <h4>Últimos Posts:</h4>

        <div class="related-posts clearfix">

          <?php
          $args = array(
        		'numberposts' => 4,
        		'category_name' => 'blog',
        		'post_type' => 'post',
        		'post_status' => 'publish'
        	);

        	$query_related = new WP_Query($args);

        	if($query_related->have_posts()): ?>
          <div class="post-grid grid-container clearfix" data-layout="fitRows">
            <?php while($query_related->have_posts()) : $query_related->the_post(); ?>
              <div class="entry clearfix">
                <div class="entry-image">
                  <a href="<?php the_permalink(); ?>"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php echo get_the_title(); ?>"></a>
                </div>
                <div class="entry-title">
                  <h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                </div>
                <ul class="entry-meta clearfix">
                  <li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
                </ul>
                <div class="entry-content">
                  <p><?php the_excerpt(); ?></p>
                  <a href="<?php the_permalink(); ?>" class="more-link">Leer más</a>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
          <?php endif; ?>

        </div>

      </div>

    </div>

  </div>

</section><!-- #content end -->
