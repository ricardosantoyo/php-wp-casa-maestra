<style>
div.wpb_wrapper p span {
	font-family: 'Lato', sans-serif !important;
}

p {
  font-size: 16px;
}

.cm-container {
  padding-left: 50px;
  padding-right: 50px;
}

#metodosPago {
  padding: 50px;
  width: 90%;
}

@media (max-width: 991px){
  .container {
    margin-right: 0;
    margin-left: 0;
    padding-left: 0;
    padding-right: 0;
    width: 100% !important;
  }
  .cm-container {
    padding-left: 0;
    padding-right: 5%;
  }

  #metodosPago {
    padding: inherit;
    width: 100%;
  }
}
</style>
<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="single-event">
      <?php
      $header = get_post_meta(get_the_ID(), 'header', true);
      $header = empty($header) ? get_the_post_thumbnail_url(get_the_ID()) : $header;
       ?>

      <div class="entry-image parallax header-stick" style="background-image: url('<?php echo $header; ?>'); background-size: contain; height:600px" data-stellar-background-ratio="0" data-stellar-vertical-offset="180">
        <div class="entry-overlay-meta">
          <h2><a href="#"><?php the_title(); ?></a></h2>
          <ul class="iconlist">
            <?php
            $modalidad = get_post_meta(get_the_ID(), 'modalidad', true);
            $duracion = get_post_meta(get_the_ID(), 'duracion', true);
            $precio = get_post_meta(get_the_ID(), 'precio');
            ?>
            <?php
            if(!empty($modalidad)) { ?>
            <li><i class="icon-calendar3"></i> <strong>Modalidad:</strong> <?php echo $modalidad; ?></li>
            <?php } else { ?>
              <li><i class="icon-calendar3"></i> <strong>Modalidad:</strong> Presencial</li>
            <?php } ?>
            <?php
            if(!empty($duracion)) { ?>
            <li><i class="icon-time"></i> <strong>Duración:</strong> <?php echo $duracion; ?></li>
            <?php } ?>
            <?php
            if(!empty($precio)) {
              foreach($precio as $p) {
                ?>
                <li><i class="icon-dollar"></i> <?php echo $p; ?></li>
              <?php }
              } ?>
          </ul>



          <button class="btn btn-danger btn-block btn-lg" data-toggle="modal" data-target=".cm-modal-pagos" style="background-color:#7eb31f; color:#fff; border:none;">¡Quiero inscribirme!</button>
        </div>

      </div>

      <div class="container topmargin clearfix">

        <div class="postcontent nobottommargin clearfix" style="margin-right: 5%; margin-left: 5%;">

          <div class="col_full cm-container">

            <p>
              <?php the_content(); ?>
            </p>

          </div>

        </div>

        <!-- Post Navigation
        ============================================= -->
        <div class="post-navigation clearfix">

          <?php get_template_part( 'nav', 'below-single' ); ?>

        </div><!-- .post-navigation end -->

      </div>

  </div>

</section>

<div class="modal fade cm-modal-pagos" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg"  style="background-color:#fff; color:515151;">
          <div class="modal-body" style="padding:0 !important;">
            <div class="modal-content" style="background-color:#fff; color:515151; box-shadow:none; border:none;">
              <div class="modal-header" style="background-color:#702982;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title" id="myModalLabel" style="color:#fff;">¡Inscribirte es muy fácil!</h2>
              </div>
              <div class="modal-body">




        <form class="nobottommargin" id="template-contactform" name="template-contactform" action="<?php bloginfo('template_directory'); ?>/include/mailcurso.php" method="post" style="background-color:#fff; color:515151;">

          <div class="form-process"></div>

          <div class="col_one_third">
            <label for="template-contactform-name">Nombre completo <small>*</small></label>
            <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required show-error-msg formRentarEspacios" />
          </div>

          <div class="col_one_third">
            <label for="template-contactform-email">Correo electrónico <small>*</small></label>
            <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control formRentarEspacios" />
          </div>

          <div class="clear"></div>

          <div class="col_two_third">
            <label for="template-contactform-phone">Teléfono <small>*</small></label>
            <input type="phone" id="template-contactform-phone" name="template-contactform-phone" value="" class="required sm-form-control formRentarEspacios" />
          </div>

          <div class="col_one_third col_last">
            <label for="template-contactform-service">Servicio</label>
            <select id="template-contactform-service" name="template-contactform-service" class="sm-form-control formRentarEspacios required">
              <option value="">-- Selecciona una opción --</option>
              <option value="Inscripción">Inscripción</option>
              <option value="información">Información</option>
              <option value="Otro">Otro</option>

            </select>
          </div>

          <div class="clear"></div>

          <div class="col_full">
            <label for="template-contactform-message">Mensaje <small>*</small></label>
            <textarea class="required sm-form-control formRentarEspacios" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
          </div>

			<div class="col_full hidden">
            <input type="hidden" id="template-contactform-referer" name="template-contactform-referer" value="<?php echo get_the_title(); ?>" class="hidden sm-form-control formRentarEspacios" />
          </div>

          <div class="col_full hidden">
            <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control formRentarEspacios" />
            <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="sm-form-control formRentarEspacios" value="<?php echo get_the_title(); ?>" />
          </div>

          <div class="col_full">
            <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">ENVIAR</button>
          </div>

        </form>




        </div>
      </div>
    </div>
  </div>
</div>
