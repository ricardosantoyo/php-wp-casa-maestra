<div class="entry clearfix">
  <div class="entry-image">
    <a href="<?php echo get_the_permalink(); ?>"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url( get_the_ID() ); ?>" alt="<?php the_title(); ?>"></a>
  </div>
  <div class="entry-c">
    <div class="entry-title">
      <h2><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </div>
    <ul class="entry-meta clearfix">
      <li><i class="icon-calendar3"></i> <?php echo the_date(); ?></li>
      <li><i class="icon-user"></i> <?php echo get_the_author_meta( 'first_name', get_the_author_ID() ) . ' ' . get_the_author_meta( 'last_name', get_the_author_ID( )); ?></li>
      <li><i class="icon-folder-open"></i> <?php
      $categories = get_the_category();
      foreach ($categories as $category) {
        $cat[] = $category->name;
      }
      $cat_string = implode($cat, ", ");
      echo $cat_string;
      ?></li>
      <li><a href="<?php echo get_the_permalink(); ?>#comment-section"><i class="icon-comments"></i> <?php echo get_comments_number( get_the_ID() ); ?></a></li>
    </ul>
    <div class="entry-content">
      <p><?php the_excerpt(); ?></p>
      <a href="<?php echo get_the_permalink(); ?>"class="more-link">Leer más...</a>
    </div>
  </div>
</div>
