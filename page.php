<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php
if(function_exists('cm_fancy_title')){
  $key = get_post_meta( get_the_ID(), 'subtitle', true );
  $subtitle = "";
    if (!empty($key)) {
        $subtitle = $key;
    }

  $no_title = get_post_meta( get_the_ID(), 'no_title', true );
  $no_title = !empty( $no_title ) ? ( ( $no_title === "true" ) ? true : false ) : false;

  $fancy = get_post_meta( get_the_ID(), 'fancy_title', true );

  $fancy = !empty( $fancy ) ? ( ( $fancy === "true" ) ? true : false ) : false;

  if(!$no_title){
    cm_fancy_title(get_the_title(), $subtitle, $fancy);
  }
}
?>

<?php
$map = get_post_meta( get_the_ID(), 'map', true );
$map = !empty( $map ) ? ( ( $map === "true" ) ? true : false ) : false;

if($map) {
  cm_contacto_mapa();
} ?>
<section id="content">

  <div class="content-wrap">

    <?php
    $full_width = get_post_meta( get_the_ID(), 'full_width', true );
    $full_width = !empty( $full_width ) ? ( ( $full_width === "true" ) ? true : false ) : false;

    if($full_width){
    ?>
    <div class="container-fluid clearfix">
    <?php } else { ?>
    <div class="container clearfix">
    <?php } ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <section class="entry-content">
        <?php the_content(); ?>
        <div class="entry-links"><?php wp_link_pages(); ?></div>
      </section>
      </article>
    </div>
  </div>
</section>
<?php edit_post_link(); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
