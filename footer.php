<!-- Footer
============================================= -->
<footer id="footer" class="dark">

  <div class="container">

    <!-- Footer Widgets
    ============================================= -->
    <div class="footer-widgets-wrap clearfix">

      <div class="col_one_third">

        <div class="widget clearfix">

          <?php
          $footer_logo = get_option("cm-footer-logo");

          if(!empty($footer_logo)) { ?>

          <img src="<?php echo $footer_logo; ?>" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" class="footer-logo">
          <?php } ?>

          <div style="background-size: 100%;">
            <address>


              <strong>Prado Norte #565</strong><br>
              Lomas de Chapultepec<br>
              México, D.F., C.P. 11000<br>
            </address>
            <abbr title="Phone Number"><strong>Teléfonos:</strong></abbr> <a href="tel:5541731477">55 4173-1477</a> <br>
            <abbr title="Email Address"><strong>Correo:</strong></abbr> <a href="mailto:info@casamaestra.com.mx">info@casamaestra.com.mx</a>




          </div>

        </div>

      </div>

      <div class="col_one_third">

        <div class="widget clearfix">
          <h4>Mamás que nos recomiendan</h4>

          <?php
          $args = array(
            'numberposts' => 3,
            'category_name' => 'testimonials',
	  		'orderby' => 'rand'
          );

          $testimonials = get_posts( $args );

          if( $testimonials ) { ?>
            <div class="fslider testimonial no-image nobg noborder noshadow nopadding" data-animation="slide" data-arrows="false">
              <div class="flexslider">
                <div class="slider-wrap">
                  <?php
                  foreach ($testimonials as $post) { ?>
                    <div class="slide">
                      <div class="testi-image">
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                      </div>
                      <div class="testi-content">
                        <p><?php echo $post->post_content; ?> </p>
                        <div class="testi-meta">
                          <?php echo $post->post_title; ?>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } ?>

        </div>

        <?php cm_footer_redes(); ?>

		<br>
		<h4>Descarga nuestra app para las Conferencias en Línea</h4>
		
		<div class="widget clearfix">
			<a href="https://play.google.com/store/apps/details?id=com.thinktalentia.casamaestraonline" target="_blank" class="social-icon si-rounded si-android">
				<i class="icon-android"></i>
				<i class="icon-android"></i>
			</a>

			<a href="https://itunes.apple.com/us/app/casa-maestra-online/id1199667187?mt=8" target="_blank" class="social-icon si-rounded si-appstore">
				<i class="icon-appstore"></i>
				<i class="icon-appstore"></i>
			</a>
		</div>

      </div>

      <div class="col_one_third col_last">

        <div class="widget quick-contact-widget clearfix">

          <h4>Envíanos un mensaje</h4>

          <div class="quick-contact-form-result"></div>

          <form id="quick-contact-form" name="quick-contact-form" action="<?php bloginfo('template_directory'); ?>/include/contactorapido.php" method="post" class="quick-contact-form nobottommargin">

            <div class="form-process"></div>

            <div class="input-group divcenter">
              <span class="input-group-addon"><i class="icon-user"></i></span>
              <input type="text" class="required form-control input-block-level" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Nombre completo" />
            </div>
            <div class="input-group divcenter">
              <span class="input-group-addon"><i class="icon-email2"></i></span>
              <input type="text" class="required form-control email input-block-level" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Correo electrónico" />
            </div>
            <textarea class="required form-control input-block-level short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="4" cols="30" placeholder="Mensaje"></textarea>
            <input type="text" class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />
            <button style="background-color:#7eb31f; border:none;" type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="btn btn-danger nomargin" value="submit">Enviar</button>

          </form>

        </div>

		<div class="clearfix"></div>

		<br />

		<h4>Suscríbete a nuestro newsletter</h4>

		<a class="button button-default button-reveal" href="https://visitor.r20.constantcontact.com/manage/optin?v=001uL6aNbggBXJV78LHQNeUTBXTUOdZiTGpXIWyA3L5RceT5rbIm0bvNWXHLEOUKi20HBiftVFdO7JCi6XmvSntcX6LSr0UNoEcWT4PDpqJS87Y0zz8fAAXI5JBCx4MIC9kys2ThDLDKszznHqDvWCO_h-Kv24Zlc-N" target="_blank"><i class="icon-email2"></i> <span>Suscribirse</span></a>

      </div>

    </div><!-- .footer-widgets-wrap end -->

  </div>

  <!-- Copyrights
  ============================================= -->
  <div id="copyrights">

    <div class="container clearfix">

      <div class="col_half">
        Copyright &copy; Todos los derechos reservados <span class="copyright-links" data-class-lg="hidden" data-class-md="hidden"><br><a href="terminos-y-condiciones">Términos y Condiciones</a><br><a href="aviso-de-privacidad">Aviso de privacidad</a></span>
      </div>

      <div class="col_half col_last tright">
        <div class="fright clearfix">
          <div class="copyrights-menu copyright-links nobottommargin">
            <a href="terminos-y-condiciones">Términos y Condiciones</a>/<a href="aviso-de-privacidad">Aviso de privacidad</a>
          </div>
        </div>
      </div>

    </div>

  </div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/functions.js"></script>

<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() {
    setTimeout(function(){
      jQuery(window).resize();
    }, 2000);
  });

  var tpj=jQuery;
  tpj.noConflict();

  tpj(document).ready(function() {

    var apiRevoSlider = tpj('.tp-banner').show().revolution(
    {
      sliderType:"standard",
      jsFileLocation:"<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/",
      sliderLayout:"fullwidth",
      dottedOverlay:"none",

      delay:8000,
      hideThumbs:50,

      thumbWidth:100,
      thumbHeight:50,
      thumbAmount:5,

      navigation: {
        keyboardNavigation:"on",
        keyboard_direction: "horizontal",
        mouseScrollNavigation:"off",
        onHoverStop:"on",
        touch:{
          touchenabled:"on",
          swipe_threshold: 75,
          swipe_min_touches: 1,
          swipe_direction: "horizontal",
          drag_block_vertical: false
        },
        arrows: {
                      style: "hermes",
                      enable: false,
                      hide_onmobile: true,
                      hide_onleave: false,
                      tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div>	<div class="tp-arr-titleholder">{{title}}</div>	</div>',
                      left: {
                          h_align: "left",
                          v_align: "center",
                          h_offset: 10,
                          v_offset: 0
                      },
                      right: {
                          h_align: "right",
                          v_align: "center",
                          h_offset: 10,
                          v_offset: 0
                      }
                  }
      },
      responsiveLevels:[1240,1024,778,480],
			visibilityLevels:[1240,1024,778,480],
			gridwidth:[1240,1024,778,480],
			gridheight:[400,400,960,600],

      touchenabled:"on",
      onHoverStop:"on",

      swipe_velocity: 0.7,
      swipe_min_touches: 1,
      swipe_max_touches: 1,
      drag_block_vertical: false,

      parallax:"mouse",
      parallaxBgFreeze:"on",
      parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

      keyboardNavigation:"off",

      navigationHAlign:"center",
      navigationVAlign:"bottom",
      navigationHOffset:0,
      navigationVOffset:20,

      soloArrowLeftHalign:"left",
      soloArrowLeftValign:"center",
      soloArrowLeftHOffset:20,
      soloArrowLeftVOffset:0,

      soloArrowRightHalign:"right",
      soloArrowRightValign:"center",
      soloArrowRightHOffset:20,
      soloArrowRightVOffset:0,

      shadow:0,
      fullWidth:"on",
      fullScreen:"off",

      spinner:"spinner4",

      stopLoop:"off",
      stopAfterLoops:-1,
      stopAtSlide:-1,

      shuffle:"off",

      autoHeight:"off",
      forceFullWidth:"off",
      hideTimerBar:"on",


      hideThumbsOnMobile:"off",
      hideNavDelayOnMobile:1500,
      hideBulletsOnMobile:"off",
      hideArrowsOnMobile:"off",
      hideThumbsUnderResolution:0,

      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      startWithSlide:0,
    });

    apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
      setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
    });

    apiRevoSlider.bind("revolution.slide.onchange",function (e,data) {
      SEMICOLON.slider.revolutionSliderMenu();
    });

  });

</script>

</body>
</html>

<!--
<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
<?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'blankslate' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); echo sprintf( __( ' Theme By: %1$s.', 'blankslate' ), '<a href="http://tidythemes.com/">TidyThemes</a>' ); ?>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
