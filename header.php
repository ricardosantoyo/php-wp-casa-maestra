<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="Ricardo Santoyo Reyes &amp; Elizabeth Gómez" />
  <meta name="keywords" content="cursos, talleres, bebes, niños, mamás, maternidad, cocina, estimulación temprana, cursos para niños, talleres para niños, escuela para padres, talleres para padres, cursos de maternidad, talleres postparto, primeros auxilios, conferencias en linea">
  <meta name="description" content="Educación Famiiar: cursos presenciales y conferencias en línea. ">

  <!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700,800,900|Roboto:400,100,300,500,700,900|Permanent+Marker" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/magnific-popup.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/calendar.css" type="text/css" />

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/include/rs-plugin/css/navigation.css">

  <?php wp_head(); ?>
<style>
@media (max-width: 1300px) {
#primary-menu ul li a {  
font-size: 12px !important;
}
}

@media (max-width: 991px) {
	img.banner-tabs {
		width: 100% !important;
	}
}

@media (min-width: 992px) and (max-width: 1271px){
#header, #header-wrap, #header.sticky-style-2, #header.sticky-style-3, #header.sticky-style-2 #header-wrap, #header.sticky-style-3 #header-wrap {
    height: auto !important;
}

#header.full-header .container, .container-fullwidth {
    padding: 0 15px !important;
    margin: 0 auto;
    width: 750px !important;
}

body:not(.top-search-open) #primary-menu-trigger {
    opacity: 1;
    -webkit-transition: opacity .2s .2s ease, top .4s ease;
    -o-transition: opacity .2s .2s ease, top .4s ease;
    transition: opacity .2s .2s ease, top .4s ease;
}

#primary-menu-trigger {
    opacity: 1;
    pointer-events: auto;
    top: 25px;
    margin-top: 0;
    left: 0;
    z-index: 1;
}

#logo {
    display: block;
    height: 100px;
    float: none;
    margin: 0 auto 0 !important;
    max-width: none;
    text-align: center;
    border: 0 !important;
    padding: 0 !important;
}

#primary-menu {
    display: block;
    float: none;
}

@primary-menu ul li > a {
	padding: 19px 15px;
}

#top-search {
    margin: 0 !important;
}

#top-search a, #top-cart, #side-panel-trigger, #top-account {
    position: absolute;
    top: 0;
    left: auto;
    right: 15px;
    margin: 40px 0;
    -webkit-transition: margin .4s ease;
    -o-transition: margin .4s ease;
    transition: margin .4s ease;
}

#top-search a {
    right: 43px;
}

#primary-menu > ul, #primary-menu > div > ul {
    display: none;
    float: none !important;
    border: 0 !important;
    padding: 0 !important;
    margin: 0 !important;
    -webkit-transition: none;
    -o-transition: none;
    transition: none;
}

#primary-menu ul li:first-child {
    border-top: 0;
}

body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul ul, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul ul li, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content ul ul, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content.col-2 > ul, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content.col-3 > ul, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content.col-4 > ul, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content.col-5 > ul, body:not(.dark) #header:not(.dark) #header-wrap:not(.dark) #primary-menu ul li .mega-menu-content.style-2 li {
    border-top-color: #EEE !important;
}

#primary-menu ul li {
    float: none;
    margin: 0 !important;
    text-align: left !important;
    border-top: 1px solid #EEE;
}

#logo > a > img {
	margin: 0 auto;
}

}
</style>
</head>
<body class="stretched">

  <div class="body-overlay"></div>

  <!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

	<!-- Header
		============================================= -->
		<header id="header" class="full-header">

      <?php create_cm_menu("main-menu"); ?>

    </header><!-- #header end -->

    <?php cm_banner_redes(); ?>
