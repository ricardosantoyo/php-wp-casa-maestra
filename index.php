<?php get_header(); ?>
<?php
if ( is_front_page() || is_home() ) {

	get_template_part( 'main', 'slider' );

} ?>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap" style="padding-top:0 !important;">

					<?php
					/*if ( is_front_page() || is_home() ) {
						$offset = (get_query_var('paged')) ? get_query_var('paged') : 0;
						get_home_posts($offset);
						?>
						<div class="col_half nobottommargin">
							<?php next_posts_link( '&lArr; Entradas Anteriores' ); ?>
						</div>
						<div class="col_half col_last tright nobottommargin">
							<?php previous_posts_link( 'Nuevas Entradas &rArr;' ); ?>
						</div>
						<?php
					}*/
					if ( is_front_page() || is_home() ) {

						//We get the main content tabs
						cm_get_tabs();

					}
					?>

			</div>

			<?php
			if ( is_front_page() || is_home() ) {

				get_template_part( 'main', 'tips' );
				get_template_part( 'main', 'espacios' );
				cm_blog_index_func();
				get_template_part( 'main', 'newsletter' );

			} ?>

		</section><!-- #content end -->

		<?php get_footer(); ?>
