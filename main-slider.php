<style>
.content-wrap {
  padding: 0;
}

.revo-slider-emphasis-text {
    font-size: 64px;
    font-weight: 700;
    letter-spacing: -1px;
    font-family: 'Raleway', sans-serif;
    padding: 15px 20px;
    border-top: 2px solid #FFF;
    border-bottom: 2px solid #FFF;
  }

  .revo-slider-desc-text {
    font-size: 20px;
    font-family: 'Lato', sans-serif;
    width: 650px;
    text-align: center;
    line-height: 1.5;
  }

  .revo-slider-caps-text {
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 3px;
    font-family: 'Raleway', sans-serif;
  }
  .tp-video-play-button { display: none !important; }

  .tp-caption { white-space: nowrap; }


  .portfolio-desc {
    height:100px !important;
  }

  .portfolio-image {
    border:1px solid #e7e7e7;
  }

  .owl-carousel {
    margin-top:50px;
  }

  .DivComoFuncionaenLinea {

    width:100%; height: 90px; margin-top:30px; margin-left:12.5%;
  }

  .comoFuncionaenLinea {
    text-align: center;
  }

  .comoFuncionaenLinea h2 {
    margin:0; padding: 0; line-height: 5px;
  }

  .comoFuncionaenLinea h2 i {
    color:#7eb31f;
    padding-right: 5px;
  }

  .comoFuncionaenLinea h2 p
  {

    margin:0 0 0 10px; padding: 0; line-height: 30px; text-align: center; font-size: 14px;

  }

  @media (max-width: 991px){
    .DivComoFuncionaenLinea {
      height: 300px;
      margin-left:10%;
    }
  }
</style>
<section id="slider" class="slider-parallax revslider-wrap clearfix">
  <div class="slider-parallax-inner">
    <!--
    #################################
      - THEMEPUNCH BANNER -
    #################################
    -->
    <div class="tp-banner-container">
      <div class="tp-banner" >
        <ul>
          <!-- SLIDE  -->
          <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="8000"  data-saveperformance="off">
            <!-- MAIN IMAGE -->
			  <!-- <?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/slider/imagen1_slider.jpg -->
            <img src="http://casamaestra.com.mx/wp-content/uploads/2018/01/Plataforma-1-1.png"  alt="educacion familiar"  data-bgposition="center bottom" data-kenburns="on" data-duration="25000" data-ease="Linear.easeNone" data-scale="100" data-scaleend="120" data-bgpositionend="center top">
            <!-- LAYERS -->

            <!-- LAYER NR. 2 -->
            <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
            data-x="[453, 453, 50, 20]"
            data-y="[150, 150, 150, 150]"
            data-fontsize="['24', '24', '30', '30']"
            data-hoffset="['0', '0', '50', '50']"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1000"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; color: #fff; white-space: nowrap;">Nos especializamos en:</div>

            <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
            data-x="[210, 210, 120 ,90 ]"
            data-y="[180, 180, 210, 210]"
            data-whitespace="['nowrap', 'nowrap', 'normal', 'normal']"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1200"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3;  white-space: nowrap; color:#fff !important;" ><center>Formar y Fortalecer Familias</center></div>

            <div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text"
            data-x="245"
            data-y="225"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1400"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; color: #333; max-width: 650px; white-space: normal;"></div>


          </li>

          <?php
          $cm_slider_image = get_option("cm-slider-image");

          if(!empty($cm_slider_image)){
            $cm_slider_text_1 = get_option("cm-slider-text-1");
            $cm_slider_text_2 = get_option("cm-slider-text-2");
            $cm_slider_button = get_option("cm-slider-button");
            $cm_slider_button_text = get_option("cm-slider-button-text");
          ?>
          <li class="dark" data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-delay="8000" data-saveperformance="off">
            <!-- MAIN IMAGE -->
            <img src="<?php echo $cm_slider_image; ?>"  alt="Promociones" data-lazyload="<?php echo $cm_slider_image; ?>" data-bgposition="center top" data-scale="cover" data-bgrepeat="no-repeat">


            <?php if(!empty($cm_slider_text_1)){ ?>
            <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
            data-x="[50, 50, 40, 50]"
            data-y="[60, 60, 60, 60]"
            data-width="['800', '800', '600', '400']"
            data-fontsize="['64', '64', '56', '56']"
            data-whitespace="['normal', 'normal', 'normal', 'normal']"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1200"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="font-size: 38px; white-space: nowrap;"><?php echo $cm_slider_text_1; ?></div>
            <?php } ?>

            <?php /* if(!empty($cm_slider_text_2)){ ?>
            <div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text tleft"
            data-x="[50, 50, 40, 50]"
            data-y="[140, 140, 220, 250]"
            data-width="['800', '800', '600', '400']"
            data-fontsize="['20', '20', '40', '40']"
            data-whitespace="['normal', 'normal', 'normal', 'normal']"
            data-lineheight="['25', '25', '45', '45']"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1400"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="max-width: 550px; white-space: normal;"><?php echo $cm_slider_text_2; ?></div>
            <?php } */ ?>

            <?php if(!empty($cm_slider_button) && !empty($cm_slider_button_text)){ ?>
            <div class="tp-caption customin ltl tp-resizeme"
            data-x="[50, 50, 40, 50]"
            data-y="[220, 220, 420, 450]"
            data-width="['800', '800', '600', '400']"
            data-fontsize="['20', '20', '40', '50']"
            data-visibility="['on', 'on', 'off', 'off']"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1550"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn"><a href="<?php echo $cm_slider_button; ?>" class="button button-border button-white button-light button-large button-rounded tright nomargin" data-class-sm="hidden" data-class-xs="hidden" data-class-xxs="hidden"><span><?php echo $cm_slider_button_text; ?></span> <i class="icon-angle-right"></i></a></div>

            <div class="tp-caption customin ltl tp-resizeme"
            data-x="[50, 50, 40, 50]"
            data-y="[220, 220, 420, 450]"
            data-width="['800', '800', '600', '400']"
            data-fontsize="['20', '20', '20', '30']"
            data-visibility="['off', 'off', 'on', 'on']"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1550"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn"><a href="<?php echo $cm_slider_button; ?>" class="button button-border button-white button-light button-xlarge button-rounded tright nomargin" style="font-size:40px;" data-class-md="hidden" data-class-lg="hidden"><span><?php echo $cm_slider_button_text; ?></span> <i class="icon-angle-right"></i></a></div>
            <?php } ?>

          </li>
          <?php } ?>

          <li class="dark" data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-delay="8000" data-saveperformance="off" >
            <!-- MAIN IMAGE -->
            <img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/slider/bg2.jpg"  alt="conocenos"  data-bgposition="left bottom" data-kenburns="on" data-duration="20000" data-ease="Linear.easeNone" data-scale="100" data-scaleend="120" data-bgpositionend="right top">
            <!-- LAYERS -->

            <div class="tp-caption customin"
            data-x="40"
            data-y="60"
            data-customin="x:0;y:0;z:0;rotationZ:0;scaleX:0.6;scaleY:0.6;skewX:0;skewY:0;s:850;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="850"
            data-start="1200"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn"><iframe src='https://www.youtube.com/embed/YK5ZRzT6mrs' width='400' height='320' style='width:480px;height:320px;border: none !important;' data-class-sm="hidden" data-class-xs="hidden" data-class-xxs="hidden"></iframe></div>


            <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
            data-x="[678, 678, 40, 40]"
            data-y="[80, 80, 150, 150]"
            data-customin="x:140;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1200"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="font-size: 40px; white-space: nowrap;">#SoyMamaComoTu</div>

            <div class="tp-caption customin ltl tp-resizeme"
            data-x="[675, 675, 40, 40]"
            data-y="270"
            data-visibility="['on', 'on', 'off', 'off']"
            data-customin="x:140;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1550"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn"><a href="nosotros" target="_self" class="button button-border button-white button-light button-large button-rounded tright nomargin" data-class-sm="hidden" data-class-xs="hidden" data-class-xxs="hidden"><span>Conócenos</span> <i class="icon-angle-right"></i></a></div>

            <div class="tp-caption customin ltl tp-resizeme"
            data-x="[675, 675, 40, 40]"
            data-y="[270, 270, 340, 340]"
            data-visibility="['off', 'off', 'on', 'on']"
            data-customin="x:140;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1550"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn"><a href="nosotros" target="_self" class="button button-border button-white button-light button-xlarge button-rounded tright nomargin" style="font-size:40px;" data-class-md="hidden" data-class-lg="hidden"><span>Conócenos</span> <i class="icon-angle-right"></i></a></div>

          </li>
        </ul>
      </div>
    </div><!-- END REVOLUTION SLIDER -->
  </div>
</section>
