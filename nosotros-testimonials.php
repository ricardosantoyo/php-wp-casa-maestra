<h3 class="center" style="color:#702982;">Mamás que nos recomiendan</h3>

<div class="container wrapper bottommargin">

  <ul class="testimonials-grid grid-3 clearfix">
    <?php
    $args = array(
      'category_name' => 'testimonials',
      'post_status' => 'publish',
      'numberposts' => 9,
	  'orderby' => 'rand'
    );

    $testimonials = get_posts($args);

    if(!empty($testimonials)){
      foreach ($testimonials as $testimonial) {
    ?>
			<li>
				<div class="testimonial">

					<div class="testi-content">
						<p><?php echo $testimonial->post_content; ?></p>
						<div class="testi-meta">
							<?php echo $testimonial->post_title; ?>
						</div>
					</div>
				</div>
			</li>
    <?php
      }
    } ?>

		</ul>

</div>
