<div id="comment-section"></div>
<?php if ( 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) return; ?>
<!-- Comments
============================================= -->
<div id="comments" class="clearfix">

  <?php
    if ( have_comments() ) :
      global $comments_by_type;
      $comments_by_type = &separate_comments( $comments );
      if ( ! empty( $comments_by_type['comment'] ) ) :
  ?>

  <h3 id="comments-title"><?php comments_number(); ?></h3>

  <!-- Comments List
  ============================================= -->
  <ol class="commentlist clearfix">

    <?php
    $args = array( 'post_ID' => get_the_ID() );
    $comments = get_comments( $args );

    foreach ($comments as $comment) {
      if($comment->comment_parent == 0){
      ?>

    <li class="comment even thread-even depth-1" id="li-comment-1">
        <div id="comment-<?php echo $comment->comment_ID; ?>" class="comment-wrap clearfix">

          <div class="comment-meta">

            <div class="comment-author vcard">

              <span class="comment-avatar clearfix">
                <?php
                $img_args = array(
                    'height'        => 60,
                    'width'         => 60,
                    'default'       => get_option( 'avatar_default', 'mystery' ),
                    'force_default' => true,
                    'rating'        => get_option( 'avatar_rating' ),
                    'alt'           => '',
                    'class'         => 'avatar avatar-60 photo avatar-default',
                );
                echo get_avatar($comment, 60, '', $img_args);
                ?>
              </span>

            </div>

          </div>

          <div class="comment-content clearfix">

            <div class="comment-author"><?php echo $comment->comment_author; ?><span><a href="<?php echo get_the_guid( get_the_ID() ) . "#comment-" . $comment->comment_ID; ?>" title="Enlace permanente al comentario"><?php echo $comment->comment_date; ?></a></span></div>

            <p><?php echo $comment->comment_content; ?></p>

            <a class='comment-reply-link' href='<?php echo get_the_guid( get_the_ID() ) . "&replytocom=" . $comment->comment_ID . "#respond"; ?>'><i class="icon-reply"></i></a>

          </div>

          <div class="clear"></div>

        </div>

        <?php
        $args = array(
          'parent' => $comment->comment_ID
        );
        $childs = get_comments( $args );

        if ($childs != null) {
          ?>
          <ul class='children'>
          <?php

          foreach ($childs as $child) {
            ?>
            <li class="comment byuser comment-author-_smcl_admin odd alt depth-2" id="li-comment-<?php echo $child->comment_ID; ?>">

              <div id="comment-<?php echo $child->comment_ID; ?>" class="comment-wrap clearfix">

                <div class="comment-meta">

                  <div class="comment-author vcard">

                    <span class="comment-avatar clearfix">
                      <?php
                      $img_args = array(
                          'height'        => 40,
                          'width'         => 40,
                          'default'       => get_option( 'avatar_default', 'mystery' ),
                          'force_default' => true,
                          'rating'        => get_option( 'avatar_rating' ),
                          'alt'           => '',
                          'class'         => 'avatar avatar-60 photo avatar-default',
                      );
                      echo get_avatar($child, 40, '', $img_args);
                      ?>
                    </span>

                  </div>

                </div>

                <div class="comment-content clearfix">

                  <div class="comment-author"><?php echo $child->comment_author; ?><span><a href="<?php echo get_the_guid( get_the_ID() ) . "#comment-" . $child->comment_ID; ?>" title="Enlace permanente al comentario"><?php echo $child->comment_date; ?></a></span></div>

                  <p><?php echo $child->comment_content; ?></p>

                </div>

                <div class="clear"></div>

              </div>

            </li>
            <?php
          }

          ?>
          </ul>
          <?php
        } ?>
    </li>
    <?php
      }
    }
    ?>
  </ol>

  <?php
  $comment_form = array(
    'title_reply' => "Deja tu <span>comentario</span>",
    'id_form' => 'commentform',
    'class_form' => 'clearfix',
    'id_submit' => 'submit-button',
  );

  ?>
</div>

<?php
endif;
endif;
?>

<style>
#comment, label {
  width: 100%;
}
</style>

<!-- Comment Form
============================================= -->
<div id="respond" class="clearfix">

<?php if ( comments_open() ) comment_form($comment_form); ?>

<script>
var submitButton = document.getElementById("submit");
submitButton.className += " button button-3d nomargin";
</script>

</div>
