<style type="text/css">

		a {
			color: #7D499B;
		}

		a:hover {
			font-weight: bolder;
			color: #7D499B;
		}
		p{
			text-align: justify;
		}

		h4 { color: #7D499B; }
</style>

<p>El aviso de privacidad forma parte del uso del sitio web <a href="index.html" target="_self">www.casamestra.com.mx</a></p>

				<p>Somos un proveedor de cursos y talleres; una de las prioridades de Casa Maestra (en adelante, Casa Maestra) es respetar la privacidad de sus usuarios y mantener segura la información y los datos personales que recolecta. El usuario responderá en todo momento por los datos proporcionados y en ningún caso  Casa Maestra  será responsable de los mismos.<br>




				Los datos personales ("Los Datos") solicitados, son tratados por  Casa Maestra (en adelante, Casa Maestra ), con domicilio en  Prado Norte #565, Lomas de Chapultepec C.P. 11000, México, con la finalidad de brindarle el servicio que nos solicita, conocer sus necesidades de productos o servicios y estar en posibilidad de ofrecerle los que más se adecue a sus preferencias. En caso de formalizar con Usted la aceptación de algún producto o servicio ofrecido por Casa Maestra sus Datos serán utilizados para el cumplimiento de las obligaciones derivadas de esa relación jurídica. </p>

				<h4>Datos personales que recabamos</h4>

				<p>Información de contacto (Nombre, Email, Dirección, Teléfono, Celular, Fax) Información financiera y medios de pago (Tarjeta de crédito, débito, cheques) Información Fiscal (RFC, Dirección de Facturación). <br>




				Los Datos serán tratados de conformidad con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento. La confidencialidad de los Datos está garantizada y los mismos están protegidos por medidas de seguridad administrativas, técnicas y físicas, para evitar su daño, pérdida, alteración, destrucción, uso, acceso o divulgación indebida. Únicamente las personas autorizadas tendrán acceso a sus Datos.</p>


				<h4>Confidencialidad de la información</h4>

				<p>Cuando se encuentre en el sitio de <a href="index.html" target="_self">www.casamestra.com.mx </a> y le pidan datos personales, usted compartirá la información solo con Casa Maestra. <br>


				Casa Maestra no compartirá la información confidencial con terceras partes, excepto que tenga autorización expresa de quienes se suscribieron, o cuando haya sido requerido por orden judicial para cumplir con las disposiciones procesales<br>


				Casa Maestra no vende ni alquila la información de los usuarios. Si los datos personales del usuario debieran ser compartidos con socios comerciales o patrocinantes, el usuario será notificado antes que estos sean recogidos o transferidos.<br>


				Casa Maestra puede difundir las estadísticas en conjunto de los usuarios (por ejemplo, el porcentaje de nuestros usuarios que son hombres o mayores a determinada edad, el tráfico que genera la página, etc.), para describir nuestros servicios y para otros propósitos lícitos en los casos que marque la ley.<br>



				Casa Maestra puede difundir la información de la cuenta en casos especiales cuando pensemos que proporcionar esta información puede servir para identificar, localizar o realizar acciones legales contra personas que pudiesen infringir las condiciones del servicio de Casa Maestra, o causar daños o interferencia sobre derechos de Casa Maestra o sus propiedades, de otros usuarios de los sitios de Casa Maestra o de cualquier otra persona que pudiese resultar perjudicada por dichas actividades. Casa Maestra  puede difundir u obtener acceso a la información de cuenta cuando, actuando de buena fe, creamos que es necesario por razones legales, administrativas o de otra índole y lo consideremos necesario para mantener, proporcionar y desarrollar nuestros productos o servicios.<br>



				Casa Maestra no asume ninguna obligación de mantener confidencial cualquier otra información que el usuario proporcione, incluyendo aquella información que el usuario proporcione a través de boletines y platicas en línea(chats), así como la información que obtenga a través de cookies que se describen en el inciso 3, lo anterior en términos de lo establecido en el artículo 109 de la Ley Federal de los Derechos de Autor y de la fracción I, del artículo 76 bis de la Ley Federal de Protección al Consumidor.<br>

				Usted, como titular de datos personales, podrá ejercitar ante Casa Maestra  los derechos de acceso, rectificación, cancelación y oposición (derechos "ARCO") establecidos en la Ley. Asimismo, podrá revocar en todo momento, el consentimiento que haya otorgado y que fuere necesario para el tratamiento de sus datos personales, así como limitar el uso o divulgación de los mismos. Lo anterior, a través del envío de su petición a nuestro responsable de Protección de datos, a quien puede contactar en el correo
				<a href="mailto:info@casamestra.com.mx">info@casamestra.com.mx</a></p>

				<p>A manera de referencia, a continuación se describen brevemente los derechos ARCO: Acceso.- que se le informe cuáles de sus datos personales están contenidos en las bases de datos de la Compañía y para qué se utilizan dichos datos personales, el origen de dichos datos y las comunicaciones que se hayan realizado con los mismos. Rectificación.- que se corrijan o actualicen sus datos personales en caso de que sean inexactos o incompletos. Usted tendrá la obligación de informar a la Compañía de los cambios que se deban hacer a sus datos personales, cuando dichos cambios solo sean de su conocimiento. Cancelación.- que sus datos personales sean dados de baja total o parcialmente de las bases de datos de la Compañía. Esta solicitud dará lugar a un periodo de bloqueo tras el cual procederá la supresión de los datos. Existen casos en los que la cancelación no será posible en términos de la Ley y otras disposiciones legales aplicables. Oposición.- oponerse por causa legítima al tratamiento de sus datos personales por parte de la Compañía. En los casos que la oposición verse sobre la recepción de ciertos comunicados, en dichos comunicados se incluirá la opción para salir de la lista de envío y dejar de recibirlos.</p>


				<h4>Consentimiento</h4>
				<p>Al proporcionar sus datos personales de manera voluntaria a la Compañía, entendemos que está de acuerdo en que los mismos sean tratados de conformidad con el presente Aviso de Privacidad. No obstante lo anterior, le informamos que en cualquier momento usted puede solicitar la cancelación de sus datos u oponerse al tratamiento llevado a cabo por la Compañía.<br>

				En términos de la Ley y su Reglamento, le informamos que, ante la negativa de respuesta a la solicitudes de derechos ARCO o inconformidad con la misma, usted puede presentar ante el Instituto Federal de Acceso a la Información y Protección de Datos, la correspondiente Solicitud de Protección de Derechos en los plazos y términos fijados por la Ley y su Reglamento.</p>

				<h4>Modificaciones al aviso de privacidad </h4>

				<p>Este aviso de privacidad podrá ser modificado de tiempo en tiempo por Casa Maestra Dichas modificaciones serán oportunamente informadas a través de nuestra página en internet http://www.casamestra.com.mx/aviso-de-privacidad, o cualquier otro medio de comunicación oral, impreso o electrónico que Casa Maestra  determine para tal efecto. <br>

				Fecha última actualización; 06 de Diciembre del 2016


				</p>
